#include <stdio.h>
#include "UIEngine.h"
#include "GameData.h"
#include "GameEngine.h"

int main(int argc, char * args[])
{
   GameEngine_Setup();

   if(UIEngine_Setup())
   {
      GameEngine_Teardown();
      return -1;
   }


   while(UIEngine_IsRunning())
   {
      UIEngine_Step();
   }

   UIEngine_Teardown();
   GameEngine_Teardown();
   printf("End of Program\n");
   return 0;
}

