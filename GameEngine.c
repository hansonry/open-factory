#include <stdlib.h>
#include <string.h>
#include "GameEngine.h"

static struct serializedBlock * EditData = NULL;
static size_t                   EditDataSize = 0;


static enum mode Mode = eM_Edit;


void GameEngine_Setup(void)
{
   Mode = eM_Edit;
   BlockEngine_Setup();
}

void GameEngine_Teardown(void)
{
   BlockEngine_Teardown();
}





static inline
void SimulationReset(void)
{
   BlockEngine_WorldDeserialize(EditData, EditDataSize);
}

static inline
void SimulationSave(void)
{
   const size_t count = BlockEngine_GetBlockCount();
   EditData = realloc(EditData, 
                      sizeof(struct serializedBlock) * count);
   EditDataSize = BlockEngine_WorldSerialize(EditData, count);
}


void GameEngine_Step(void)
{
   if(Mode == eM_Playing)
   {
      BlockEngine_Step();
   }
}


void GameEngine_Clear(void)
{
   BlockEngine_Clear();   
}

void GameEngine_BlockAdd(const struct blockType * type, int x, int y, int z,
                                                        enum blockFacing facing)
{
   if(Mode == eM_Edit)
   {
      BlockEngine_BlockAdd(type, x, y, z, facing);
   }
}

bool GameEngine_BlockJoin(int x1, int y1, int z1, int x2, int y2, int z2)
{
   return BlockEngine_BlockJoin(x1, y1, z1, x2, y2, z2);
}


const struct blockType * GameEngine_GetBlockAt(int x, int y, int z, 
                                               enum blockFacing * facing)
{
   return BlockEngine_GetBlockAt(x, y, z, facing);
}


bool GameEngine_BlockRemove(int x, int y, int z)
{
   return BlockEngine_BlockRemove(x, y, z);
}


bool GameEngine_BlockSetImmovable(int x, int y, int z, bool immovable)
{
   return BlockEngine_BlockSetImmovable(x, y, z, immovable);
}

void GameEngine_SetMode(enum mode mode)
{
   if(Mode != eM_Edit && mode == eM_Edit)
   {
      SimulationReset();
   }
   if(mode == eM_Playing)
   {
      SimulationSave();
   }
   Mode = mode;
}

enum mode GameEngine_GetMode(void)
{
   return Mode;
}





