#ifndef __UIENGINE_H__
#define __UIENGINE_H__
#include <stdbool.h>
#include "GameData.h"
#include "GameEngine.h"


struct uiEngineEntity;

bool UIEngine_Setup(void);

void UIEngine_Teardown(void);

bool UIEngine_IsRunning(void);
void UIEngine_Step(void);

const struct blockType * UIEngine_GetSelectedBlockType(void);

struct uiEngineEntity * UIEngine_Entity_Create(const struct blockType * type);
void UIEngine_Entity_Destroy(struct uiEngineEntity * entity);

void UIEngine_Entity_Move(struct uiEngineEntity * entity, int x, int y, int z);
void UIEngine_Entity_Face(struct uiEngineEntity * entity, 
                          enum blockFacing facing);
void UIEngine_Entity_Activate(struct uiEngineEntity * entity, bool isActive);

const struct blockType * UIEngine_Entity_GetType(struct uiEngineEntity * entity);






#endif // __UIENGINE_H__

