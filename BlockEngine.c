#include <stdlib.h>
#include <string.h>
#include "BlockEngine.h"
#include "UIEngine.h"
#include "Position.h"

#define DECLCAST(type, dest, src) type * dest = (type *)src;

struct entityGroup;

struct entity
{
   struct uiEngineEntity * gfx;
   const struct blockType * type;
   struct entityGroup * group;
   struct position offset;
   enum blockFacing facing;
   bool immovable;
};

struct entityRotator
{
   struct entity parent;
   struct entityGroup  * lastRotated;
};

struct entityActivatable
{
   struct entity parent;
   bool isActive;
   bool isNextActive;
   unsigned int group;
};

struct entityGroup
{
   size_t refCount;
   size_t entityCount;
   struct position position;
   struct position next;
   bool immovable;
   bool rotated;
};

#define ENTITYLIST_GROWBY 32

struct entityList
{
   struct entity ** base;
   size_t count;
   size_t size;
};

static inline
struct entityGroup * EntityGroup_Create(void)
{
   struct entityGroup * group = malloc(sizeof(struct entityGroup));
   group->refCount = 1;
   group->entityCount = 0;
   group->immovable = false;
   group->rotated = false;
   PositionSet(&group->position, 0, 0, 0);
   PositionSet(&group->next,     0, 0, 0);
   return group;
}

static inline
void EntityGroup_Take(struct entityGroup * group)
{
   group->refCount ++;
}

static inline
void EntityGroup_Release(struct entityGroup * group)
{
   if(group->refCount > 1)
   {
      group->refCount --;
   }
   else
   {
      free(group);
   }
}

enum entityPositionType
{
   eEPT_Current,
   eEPT_Next
};

static inline
struct position * Entity_GetPositionCurrent(const struct entity * entity, struct position * position)
{
   PositionCopy(position, &entity->group->position);
   PositionAdd(position, &entity->offset);
   return position;
}

static inline
struct position * Entity_GetPositionNext(const struct entity * entity, struct position * position)
{
   PositionCopy(position, &entity->group->next);
   PositionAdd(position, &entity->offset);
   return position;
}

static inline
struct position * Entity_GetPosition(const struct entity * entity, 
                                     struct position * position, 
                                     enum entityPositionType type)
{
   if(type == eEPT_Current)
   {
      return Entity_GetPositionCurrent(entity, position);
   }
   return Entity_GetPositionNext(entity, position);
}

static inline
void EntityGroup_NextReset(struct entityGroup * group)
{
   PositionCopy(&group->next, &group->position);
   group->rotated = false;
}

static inline
void EntityGroup_Teleport(const struct entityList * list, struct entityGroup * group, int x, int y, int z)
{
   size_t i;
   struct position target;
   PositionSet(&target, x, y, z);
   if(!PositionIsEqual(&target, &group->position))
   {
      PositionCopy(&group->position, &target);
      for(i = 0; i < list->count; i++)
      {
         struct entity * entity = list->base[i];
         if(entity->group == group)
         {
            struct position pos;
            Entity_GetPositionCurrent(entity, &pos);
            UIEngine_Entity_Move(entity->gfx, pos.x, pos.y, pos.z);
         }
      }
      EntityGroup_NextReset(group);
   }
}

static inline
int EntityGroup_GetMinZ(const struct entityList * list, const struct entityGroup * group)
{
   size_t i;
   int minZ = 0;
   bool initialized = false;
   for(i = 0; i < list->count; i++)
   {
      struct entity * entity = list->base[i];
      if(entity->group == group)
      {
         if(!initialized || entity->offset.z < minZ)
         {
            minZ = entity->offset.z;
            initialized = true;
         }
      }
   }   
   return minZ + group->position.z;
}


static inline
bool EntityGroup_NextIsUnchanged(const struct entityGroup * group)
{
   return PositionIsEqual(&group->next, &group->position) &&
          !group->rotated;
}


struct specialTypes
{
   const struct blockType * conveyor;
   const struct blockType * duplicator;
   const struct blockType * grinder;
   const struct blockType * welder;
   const struct blockType * rotatorCW;
   const struct blockType * rotatorCCW;
   const struct blockType * wire;
   const struct blockType * sensor;
   const struct blockType * pusher;
};

static struct specialTypes BlockTypes;



static inline
void EntityListInit(struct entityList * list)
{
   list->size = ENTITYLIST_GROWBY;
   list->count = 0;
   list->base = malloc(sizeof(struct entity *) * list->size);
}
static inline
void EntityListDestroy(struct entityList * list)
{
   free(list->base);
   list->base = NULL;
   list->size = 0;
   list->count = 0;
}

static inline
void EntityListAdd(struct entityList * list, struct entity * entity)
{
   size_t newIndex;
   if(list->count >= list->size)
   {
      list->size += ENTITYLIST_GROWBY;
      list->base = realloc(list->base, 
                           sizeof(struct entity *) * list->size);
   }
   newIndex = list->count;
   list->count ++;

   list->base[newIndex] = entity;
}

static inline
bool EntityListFind(const struct entityList * list, 
                    const struct entity * entity, 
                    size_t * index)
{
   size_t i;
   for(i = 0; i < list->count; i++)
   {
      if(list->base[i] == entity)
      {
         if(index != NULL)
         {
            (*index) = i;
         }
         return true;
      }
   }
   return false;
}

static inline
struct entity * EntityListFindByPosition(const struct entityList * list,
                                         const struct position * position,
                                         size_t * index)
{
   size_t i;
   for(i = 0; i < list->count; i++)
   {
      struct position entityPos;
      Entity_GetPositionCurrent(list->base[i], &entityPos);
      if(PositionIsEqual(&entityPos, position))
      {
         if(index != NULL)
         {
            (*index) = i;
         }
         return list->base[i];
      }
   }
   return NULL;
}


static inline
bool EntityListSwap(struct entityList * list, size_t index1, size_t index2)
{
   struct entity * temp;
   if(index1 >= list->count || index2 >= list->count)
   {
      return false;
   }

   if(index1 == index2)
   {
      return true;
   }

   temp = list->base[index1];
   list->base[index1] = list->base[index2];
   list->base[index2] = temp;

   return true;
}

static inline
bool EntityListShift(struct entityList * list, 
                     size_t fromIndex, size_t toIndex, size_t size,
                     struct entity ** temp)
{
   const size_t chunksize = sizeof(struct entity *) * size;
   bool freeFlag = false;
   if(fromIndex == toIndex || size == 0)
   {
      return true;
   }

   if(fromIndex + size > list->count ||
      toIndex          > list->count)
   {
      return false;
   }

   if(temp == NULL)
   {
      temp = malloc(chunksize);
      freeFlag = true;
   }


   memcpy(temp, &list->base[fromIndex], chunksize);
   if(toIndex > fromIndex)
   {
      // fxxxx....t
      size_t shiftSize = toIndex - fromIndex;
      toIndex -= size - 1;
      memmove(&list->base[fromIndex], 
              &list->base[fromIndex + size], 
              sizeof(struct entity * ) * shiftSize);
   }
   else
   {
      //t.....fxxxx
      size_t shiftSize = fromIndex - toIndex;
      memmove(&list->base[toIndex + size], 
              &list->base[toIndex], 
              sizeof(struct entity * ) * shiftSize);
   }
   memcpy(&list->base[toIndex], temp, chunksize);

   if(freeFlag)
   {
      free(temp);
   }

   return true;
}

static inline
void EntityListRemove(struct entityList * list, const struct entity * entity)
{
   size_t index;
   if(EntityListFind(list, entity, &index))
   {
      struct entity * temp;
      
      EntityListShift(list, index, list->count - 1, 1, &temp);
      list->count --;
   }
}

static inline
size_t EntityListFindFirstInGroup2(const struct entityList * list,
                                   const struct entityGroup * group)
{
   size_t i;
   for(i = 0; i < list->count; i++)
   {
      if(list->base[i]->group == group)
      {
         return i;
      }
   }
   return list->count;
}

static inline
size_t EntityListFindFirstInGroup(const struct entityList * list, size_t index)
{
   if(index == 0)
   {
      return 0;
   }
   else
   {
      size_t i;
      struct entityGroup * group = list->base[index]->group;
      for(i = index - 1; i < list->count; i --)
      {
         if(list->base[i]->group != group)
         {
            return i + 1;
         }
      }
      return 0;
   }
}

static inline
size_t EntityListFindLastInGroup(const struct entityList * list, size_t index)
{
   size_t i;
   struct entityGroup * group = list->base[index]->group;
   for(i = index + 1; i < list->count; i++)
   {
      if(list->base[i]->group != group)
      {
         return i - 1;
      }
   }
   return list->count - 1;
}

static struct entityList PlayList;
static struct serializedBlock * EditData = NULL;
static size_t                   EditDataSize = 0;



static inline
void SetupBlockTypes(void)
{
   BlockTypes.conveyor   = GameData_GetTypeByName("conveyor");
   BlockTypes.duplicator = GameData_GetTypeByName("duplicator");
   BlockTypes.grinder    = GameData_GetTypeByName("grinder");
   BlockTypes.welder     = GameData_GetTypeByName("welder");
   BlockTypes.rotatorCW  = GameData_GetTypeByName("rotator-cw");
   BlockTypes.rotatorCCW = GameData_GetTypeByName("rotator-ccw");
   BlockTypes.wire       = GameData_GetTypeByName("wire");
   BlockTypes.sensor     = GameData_GetTypeByName("sensor");
   BlockTypes.pusher     = GameData_GetTypeByName("pusher");
}

void BlockEngine_Setup(void)
{
   EntityListInit(&PlayList);
   SetupBlockTypes();
}

void BlockEngine_Teardown(void)
{
   BlockEngine_Clear();
   EntityListDestroy(&PlayList);
   if(EditData != NULL)
   {
      free(EditData);
      EditData = NULL;
   }
}

static inline
bool BlockType_IsRotator(const struct blockType * type)
{
   return type == BlockTypes.rotatorCW ||
          type == BlockTypes.rotatorCCW;
}

static inline
bool BlockType_IsActivatable(const struct blockType * type)
{
   return type == BlockTypes.wire   ||
          type == BlockTypes.sensor ||
          type == BlockTypes.pusher;
}

static inline
bool Entity_IsRotator(const struct entity * entity)
{
   return BlockType_IsRotator(entity->type);
}


static inline
struct entity * FindBlock(const struct entityList * list,
                          const struct entityGroup * ignore, 
                          const struct entity * entity, 
                          int xOffset, int yOffset, int zOffset,
                          enum entityPositionType sourceType,
                          enum entityPositionType matchType,
                          size_t * foundIndex)
{
   size_t i;
   struct position target;
   if(entity != NULL)
   {
      Entity_GetPosition(entity, &target, sourceType);
   }
   else
   {
      PositionSet(&target, 0, 0, 0);  
   }
   PositionAddInt(&target, xOffset, yOffset, zOffset);
   for(i = 0; i < list->count; i++)
   {
      struct entity * entity = list->base[i];
      struct position pos;
      (void)Entity_GetPosition(entity, &pos, matchType);
      
      if(PositionIsEqual(&target, &pos) && entity->group != ignore)
      {
         if(foundIndex != NULL)
         {
            (*foundIndex) = i;
         }
         return entity;
      }
   }
   return NULL;
}




static inline
void SimulationReset(void)
{
   BlockEngine_WorldDeserialize(EditData, EditDataSize);
}

static inline
void SimulationSave(void)
{
   EditData = realloc(EditData, 
                      sizeof(struct serializedBlock) * PlayList.count);
   EditDataSize = BlockEngine_WorldSerialize(EditData, PlayList.count);
}


static inline
bool Entity_CanMove(const struct entity * entity)
{
   return !entity->immovable &&
          !entity->group->immovable && 
          EntityGroup_NextIsUnchanged(entity->group);
}

static inline
bool Entity_MoveProposed(const struct entity * entity)
{
   return !EntityGroup_NextIsUnchanged(entity->group);
}

static inline
struct position * Entity_ComputeGroupNextDifference(const struct entity * entity,
                                                    struct position * diff)
{
   PositionCopy(diff, &entity->group->next);
   PositionSubtract(diff, &entity->group->position);
   return diff;
}

static inline
bool Entity_NextIsHorisontalMove(const struct entity * entity)
{
   struct position diff;
   (void)Entity_ComputeGroupNextDifference(entity, &diff);
   return diff.z == 0 && (diff.x != 0 || diff.y != 0);
}

static inline
void Entity_CopyGroupNext(struct entity * dest, const struct entity * src)
{
   struct position diff;
   (void)Entity_ComputeGroupNextDifference(src, &diff);
   EntityGroup_NextReset(dest->group);
   PositionAdd(&dest->group->next, &diff);
}

static inline
struct entity * CreateEntity(struct entityList * list, 
                             const struct blockType * type,
                             int x, int y, int z, 
                             enum blockFacing facing);

static inline
void SimulateBlockCreation(struct entityList * list)
{
   size_t i;
   for(i = 0; i < list->count; i++)
   {
      struct entity * entity = list->base[i];
      if(entity->type == BlockTypes.duplicator)
      {
         struct entity * source = FindBlock(list, NULL, entity,
                                            0, 0, 1,
                                            eEPT_Current,
                                            eEPT_Current,
                                            NULL);
         if(source != NULL)
         {
            struct position pos;
            Entity_GetPositionCurrent(entity, &pos);
            pos.z -= 1;
            if(BlockEngine_GetBlockAt(pos.x, pos.y, pos.z, NULL) == NULL)
            {
               (void)CreateEntity(&PlayList, source->type, 
                                  pos.x, pos.y, pos.z,
                                  source->facing);
            }
         }         
      }
   }
}

static inline
void SimulateGravity(struct entityList * list)
{
   size_t i;
   for(i = 0; i < list->count; i++)
   {
      struct entity * entity = list->base[i];
      if(Entity_CanMove(entity))
      {
         int minZ = EntityGroup_GetMinZ(list, entity->group);
         if(minZ > 0)
         {
            PositionAddInt(&entity->group->next, 0, 0, -1);
         }
      }
   }
   for(i = 0; i < list->count; i++)
   {
      struct entity * entity = list->base[i];
      if(Entity_MoveProposed(entity))
      {
         struct entity * overlap = FindBlock(list, entity->group, entity,
                                             0, 0, 0,
                                             eEPT_Next,
                                             eEPT_Next,
                                             NULL);

         if(overlap != NULL)
         {
            while(entity != NULL)
            {
               EntityGroup_NextReset(entity->group);
               entity = FindBlock(list, entity->group, entity, 
                                  0, 0, 0, eEPT_Next, eEPT_Next, NULL);
            }
         }
      }
   }
}

static inline
void SimulateConveyer(struct entityList * list)
{
   size_t i;
   bool searchAgain;

   // Get pushed by conveyers
   for(i = 0; i < list->count; i++)
   {
      struct entity * entity = list->base[i];
      if(Entity_CanMove(entity))
      {
         struct entity * below = FindBlock(list, entity->group, entity,
                                           0, 0, -1,
                                           eEPT_Next, eEPT_Next, NULL);
         if(below != NULL && below->type == BlockTypes.conveyor)
         {
            switch(below->facing)
            { 
            case eBF_North:
               PositionAddInt(&entity->group->next,  0, -1, 0);
               break;
            case eBF_East:
               PositionAddInt(&entity->group->next,  1,  0, 0);
               break;
            case eBF_South:
               PositionAddInt(&entity->group->next,  0,  1, 0);
               break;
            case eBF_West:
               PositionAddInt(&entity->group->next, -1,  0, 0);
               break;
            }
         }
      }
   }

   // Get pushed by other blocks getting push by conveyers
   searchAgain = true;
   while(searchAgain)
   {
      searchAgain = false;
      for(i = 0; i < list->count; i++)
      {
         struct entity * entity = list->base[i];
         if(Entity_CanMove(entity))
         {
            struct entity * overlap = FindBlock(list, entity->group, entity,
                                                0, 0, 0,
                                                eEPT_Next, eEPT_Next, NULL);
            if(overlap != NULL && 
               Entity_NextIsHorisontalMove(overlap))
            {
               Entity_CopyGroupNext(entity, overlap);
               searchAgain = true;
            }
         }
      }
   }


   // Check to see if you have run into anything, if you have,
   // back it up.
   for(i = 0; i < list->count; i++)
   {
      struct entity * entity = list->base[i];
      if(Entity_MoveProposed(entity))
      {
         struct entity * overlap = FindBlock(list, entity->group, entity,
                                             0, 0, 0,
                                             eEPT_Next, eEPT_Next, NULL);
         if(overlap != NULL)
         {
            while(entity != NULL)
            {
               EntityGroup_NextReset(entity->group);
               entity = FindBlock(list, entity->group, entity, 
                                  0, 0, 0,
                                  eEPT_Next, eEPT_Next, NULL);
            }
         }
      }
   }
}

static inline
bool BlockEngine_RotateGroup(struct entityList * list, size_t index, 
                              enum blockRotation rotation);

static inline
bool EntityRotator_CanRotate(const struct entityRotator * rotator, 
                             const struct entity * rotated)
{
   return Entity_CanMove(rotated) &&
          rotator->lastRotated != rotated->group;
          
}

static inline
void EntityRotator_SetLastRotated(struct entityRotator * rotator,
                                  struct entityGroup * rotatedGroup)
{
   if(rotator->lastRotated != NULL)
   {
      EntityGroup_Release(rotator->lastRotated);
   }
   rotator->lastRotated = rotatedGroup;
   if(rotatedGroup != NULL)
   {
      EntityGroup_Take(rotatedGroup);
   }
}


// From testing Infinifactory
// 1. Rotator gets highest prioity, even if belts are touching other
//    parts of the group.
// 2. The rotator can only rotate the group once. This is only cleared if
//    the group leaves the rotator. Even if belts push new parts of the 
//    group across the rotator.
// 3. You cannot rotate a group and have a group effected by a belt in the
//    same step.
static inline
void SimulateRotation(struct entityList * list)
{
   size_t i;
   for(i = 0; i < list->count; i++)
   {
      struct entity * entity = list->base[i];
      if(Entity_IsRotator(entity))
      {
         DECLCAST(struct entityRotator, rotator, entity);
         size_t index;
         struct entity * rotated = FindBlock(list, 
                                             rotator->parent.group, 
                                             &rotator->parent,
                                             0, 0, 1, 
                                             eEPT_Next, eEPT_Current, &index);
         if(rotated != NULL)
         {
            if(EntityRotator_CanRotate(rotator, rotated))
            {               
               enum blockRotation rotation = (rotator->parent.type == BlockTypes.rotatorCW) ?
                                             eBR_Right : eBR_Left;
               if(BlockEngine_RotateGroup(list, index, rotation))
               {
                  rotated->group->rotated = true;                  
                  EntityRotator_SetLastRotated(rotator, rotated->group);
               }
            }
         
         }
         else
         {
            EntityRotator_SetLastRotated(rotator, NULL);
         }

      }
   }

}

static inline
void SimulateResetAllGroups(struct entityList * list)
{
   size_t i;
   // Initialize next Position
   for(i = 0; i < list->count; i++)
   {
      EntityGroup_NextReset(list->base[i]->group);
   }
}


static inline
void SimulationLockInNewPositions(struct entityList * list)
{
   size_t i;
   for(i = 0; i < list->count; i++)
   {
      struct entity * entity = list->base[i];
      if(Entity_MoveProposed(entity))
      {
         EntityGroup_Teleport(&PlayList, 
                              entity->group,
                              entity->group->next.x,
                              entity->group->next.y,
                              entity->group->next.z);
      }
   }
}


static inline
void BlockEngine_RemoveEntity(struct entityList * list, struct entity * entity);

static inline
void SimulationBlockDestruction(struct entityList * list)
{
   size_t i;
   for(i = list->count - 1; i < list->count; i--)
   {
      struct entity * entity = list->base[i];
      if(entity->type == BlockTypes.grinder)
      {
         struct position offset;
         struct entity * target;
         PositionGetRotatedPoint(&offset, 0, -1, 0, entity->facing);
         target = FindBlock(list, entity->group, entity,
                            offset.x, offset.y, offset.z,
                            eEPT_Current, eEPT_Current, NULL);
         if(target != NULL)
         {
            BlockEngine_RemoveEntity(list, target); 
         }
         
      }
   } 
}

static inline
void Entity_GetRotatedRealativePoint(const struct entity * entity, 
                                     struct position * position, 
                                     enum entityPositionType positionType,
                                     int x, int y, int z)
{
   struct position offset;
   Entity_GetPosition(entity, position, positionType);
   PositionGetRotatedPoint(&offset, x, y, z, entity->facing);
   PositionAdd(position, &offset);
}

static inline
void SimulateWeld(struct entityList  * list)
{
   size_t i, k;
   for(i = 0; i < list->count; i++)
   {
      struct entity * entity1 = list->base[i];
      if(entity1->type == BlockTypes.welder)
      {
         struct position pos1;
         Entity_GetRotatedRealativePoint(entity1, &pos1, eEPT_Current, 
                                         0, -1, 0);
         for(k = i + 1; k < list->count; k++)
         {
            struct entity * entity2 = list->base[k];
            if(entity2->type == BlockTypes.welder)
            {
               struct position pos2, diff;
               Entity_GetRotatedRealativePoint(entity2, &pos2, eEPT_Current, 
                                               0, -1, 0);
               PositionCopy(&diff, &pos1);
               PositionSubtract(&diff, &pos2);
               if(PositionManhattanLength(&diff) == 1)
               {
                  BlockEngine_BlockJoin(pos1.x, pos1.y, pos1.z,
                                       pos2.x, pos2.y, pos2.z);
               }
            }
         }
      }
   }
}

static inline
void EntityListRegroupActivable(struct entityList * list, unsigned int from, 
                                                          unsigned int to)
{
   size_t i;
   for(i = 0; i < list->count; i++)
   {
      struct entity * entity = list->base[i];
      if(BlockType_IsActivatable(entity->type))
      {
         DECLCAST(struct entityActivatable, activatable, entity);
         if(activatable->group == from)
         {
            activatable->group = to;
         }
      }
   }
}

static inline
void SimulateCreateActivatableGroups(struct entityList * list)
{
   size_t i;
   unsigned int nextGroup = 0;
   // Assign everyone a unique group
   for(i = 0; i < list->count; i++)
   {
      struct entity * entity = list->base[i];
      if(BlockType_IsActivatable(entity->type))
      {
         DECLCAST(struct entityActivatable, activatable, entity);
         activatable->group = nextGroup;
         activatable->isNextActive = false;
         nextGroup ++;
      }
   }

   // Compair every two blocks, see if they are ajesent, if they are, join groups.
   for(i = 0; i < list->count; i++)
   {
      struct entity * iEntity = list->base[i];
      if(BlockType_IsActivatable(iEntity->type))
      {
         DECLCAST(struct entityActivatable, iActivatable, iEntity);
         struct position iPos;
         Entity_GetPositionCurrent(&iActivatable->parent, &iPos);
         size_t k;
         for(k = i + 1; k < list->count; k++)
         {
            struct entity * kEntity = list->base[k];
            if(BlockType_IsActivatable(kEntity->type))
            {
               DECLCAST(struct entityActivatable, kActivatable, kEntity);
               struct position diff;
               Entity_GetPositionCurrent(&kActivatable->parent, &diff);
               PositionSubtract(&diff, &iPos);
               if(PositionManhattanLength(&diff) == 1 &&
                  kActivatable->group != iActivatable->group)
               {
                  EntityListRegroupActivable(list, kActivatable->group, 
                                                   iActivatable->group);
               }
            }
         }
      }
   }
}

static inline
void EntityListActivatableSetNext(struct entityList * list, unsigned int group, 
                                                            bool value)
{
   size_t i;
   for(i = 0; i < list->count; i++)
   {
      struct entity * entity = list->base[i];
      if(BlockType_IsActivatable(entity->type))
      {
         DECLCAST(struct entityActivatable, activatable, entity);
         if(activatable->group == group)
         {
            activatable->isNextActive = value;
         }
      }
   }
}

static inline
void EntityListActivatableNotifyUILayer(struct entityList * list)
{
   size_t i;
   for(i = 0; i < list->count; i++)
   {
      struct entity * entity = list->base[i];
      if(BlockType_IsActivatable(entity->type))
      {
         DECLCAST(struct entityActivatable, activatable, entity);
         if(activatable->isNextActive != activatable->isActive)
         {
            UIEngine_Entity_Activate(activatable->parent.gfx, 
                                     activatable->isNextActive);
            activatable->isActive = activatable->isNextActive;
         }
      }
   }
}

static inline
void SimulateCheckActivatable(struct entityList * list)
{
   size_t i;
   for(i = 0; i < list->count; i++)
   {
      struct entity * entity = list->base[i];
      if(entity->type == BlockTypes.sensor)
      {
         DECLCAST(struct entityActivatable, activatable, entity);
         struct position offset;
         struct entity * target;
         bool shouldBeActive;
         PositionGetRotatedPoint(&offset, 0, -1, 0, entity->facing);
         target = FindBlock(list, entity->group, entity,
                            offset.x, offset.y, offset.z,
                            eEPT_Current, eEPT_Current, NULL);
         shouldBeActive = target != NULL;
         if(shouldBeActive)
         {
            EntityListActivatableSetNext(list, activatable->group, true);
         }
      } 
   }
}

static inline 
void SimulationStep(struct entityList * list)
{
   SimulateBlockCreation(list);   

   SimulateResetAllGroups(list);

   SimulateGravity(list);
   
   SimulateCreateActivatableGroups(list);
   SimulateCheckActivatable(list);
   EntityListActivatableNotifyUILayer(list);
   
   SimulateRotation(list);
   SimulateConveyer(list);

   SimulationLockInNewPositions(list);
   
   SimulationBlockDestruction(list);

   SimulateWeld(list);
}

void BlockEngine_Step(void)
{
   SimulationStep(&PlayList);
}

static inline
struct entity * Entity_Init(struct entity * entity, 
                            const struct blockType * type,
                            struct entityGroup * group)
{
   entity->type  = type;
   entity->group = group;

   group->entityCount ++;
   EntityGroup_Take(group);

   entity->gfx = UIEngine_Entity_Create(type);   
   entity->immovable = type->immovable;
   PositionSet(&entity->offset, 0, 0, 0);

   if(entity->immovable)
   {
      group->immovable = true;
   }

   return entity;
}


static inline
struct entity * Entity_New(const struct blockType * type,
                           struct entityGroup * group)
{
   struct entity * entity;
   if(BlockType_IsRotator(type))
   {
      struct entityRotator * rotator = malloc(sizeof(struct entityRotator));
      rotator->lastRotated = NULL;
      entity = &rotator->parent;
   }
   else if(BlockType_IsActivatable(type))
   {
      struct entityActivatable * activatable = malloc(sizeof(struct entityActivatable));
      activatable->isActive     = false;
      activatable->isNextActive = false;
      activatable->group        = 0;
      entity = &activatable->parent;
   }
   else
   {
      entity = malloc(sizeof(struct entity));
   }

   return Entity_Init(entity, type, group);
}

static inline
struct entity * CreateEntity(struct entityList * list, 
                             const struct blockType * type,
                             int x, int y, int z, 
                             enum blockFacing facing)
{
   struct entityGroup * group = EntityGroup_Create();
   struct entity * entity = Entity_New(type, group);
   EntityGroup_Release(group);
   EntityListAdd(list, entity);
   
   EntityGroup_Teleport(list, group, x, y, z);

   entity->facing = facing;
   UIEngine_Entity_Face(entity->gfx, facing);
   return entity;
}

static inline
void EntityRemove(struct entityList * list, struct entity * entity)
{
   entity->group->entityCount --;
   EntityGroup_Release(entity->group);
   UIEngine_Entity_Destroy(entity->gfx);
   EntityListRemove(list, entity);

   if(Entity_IsRotator(entity))
   {
      DECLCAST(struct entityRotator, rotator, entity);
      EntityRotator_SetLastRotated(rotator, NULL);
   }
   free(entity);
}

void BlockEngine_Clear(void)
{
   size_t i; 
   for(i = PlayList.count - 1; i < PlayList.count; i--)
   {
      EntityRemove(&PlayList, PlayList.base[i]);
   }
}

struct stickyCheckRecord
{
   struct position offset;
   enum blockDirections current;
   enum blockDirections other;
};

static const struct stickyCheckRecord StickyData[6] = {
   { {  0,  0,  1 }, eBD_ZPos, eBD_ZNeg },
   { {  0,  0, -1 }, eBD_ZNeg, eBD_ZPos },
   { {  0,  1,  0 }, eBD_YPos, eBD_YNeg },
   { {  0, -1,  0 }, eBD_YNeg, eBD_YPos },
   { {  1,  0,  0 }, eBD_XPos, eBD_XNeg },
   { { -1,  0,  0 }, eBD_XNeg, eBD_XPos },
};

static const enum blockDirections DirRotationTable[] = {
   eBD_XPos, eBD_XNeg, eBD_YPos, eBD_YNeg, eBD_ZPos, eBD_ZNeg,
   eBD_YPos, eBD_YNeg, eBD_XNeg, eBD_XPos, eBD_ZPos, eBD_ZNeg,
   eBD_XNeg, eBD_XPos, eBD_YNeg, eBD_YPos, eBD_ZPos, eBD_ZNeg,
   eBD_YNeg, eBD_YPos, eBD_XPos, eBD_XNeg, eBD_ZPos, eBD_ZNeg,
   
};

static inline
enum blockDirections GetRotateDirection(enum blockDirections dir, 
                                        enum blockFacing facing)
{
   return DirRotationTable[dir + (facing * 6)];
}

static inline
bool EntityIsSideSticky(const struct entity * entity, 
                        enum blockDirections dir)
{
   enum blockDirections rotDir = GetRotateDirection(dir, entity->facing);
   return BlockFlag_IsSticky(entity->type->blockFlags[rotDir]);
}

static inline
void BlockEngine_StickEntity(struct entity * entity)
{

   size_t i;
   for(i = 0; i < 6; i++)
   {
      const struct stickyCheckRecord * record = &StickyData[i];
      struct entity * other = FindBlock(&PlayList, entity->group, entity,
                                        record->offset.x, 
                                        record->offset.y,
                                        record->offset.z,
                                        eEPT_Current, eEPT_Current, NULL);
      if(other != NULL && 
         EntityIsSideSticky(entity, record->current) &&
         EntityIsSideSticky(other,  record->other))
      {
         struct position entityPos, otherPos;
         Entity_GetPositionCurrent(entity, &entityPos);
         Entity_GetPositionCurrent(other, &otherPos);
         BlockEngine_BlockJoin(entityPos.x, entityPos.y, entityPos.z,
                               otherPos.x,  otherPos.y,  otherPos.z);
      }

   }
}

void BlockEngine_BlockAdd(const struct blockType * type, int x, int y, int z,
                                                        enum blockFacing facing)
{
   if(BlockEngine_GetBlockAt(x, y, z, NULL) == NULL)
   {
      struct entity * entity = CreateEntity(&PlayList, type, x, y, z, facing);
      BlockEngine_StickEntity(entity);
   }
}

static inline
void EntityJoin(struct entityList * list, const size_t index1Start, 
                                          const size_t index2Start)
{
   // Second group gets merged into first group
   // Step 1: Group reassignment and offset change
   const size_t index2End = EntityListFindLastInGroup(list, index2Start);
   const size_t size2 = index2End - index2Start + 1;
   size_t i;
   struct entityGroup * group1 = list->base[index1Start]->group;
   struct entityGroup * group2 = list->base[index2Start]->group;
   struct position positionDiff;
   (void)PositionCopy(&positionDiff, &group2->position);
   (void)PositionSubtract(&positionDiff, &group1->position);

   // We are going to delete group2 but we want to take it to check for 
   // hanging rotator references.
   EntityGroup_Take(group2);

   if(group2->immovable)
   {
      group1->immovable = true;
   }
   for(i = index2Start; i <= index2End; i++)
   {
      struct entity * entity2 = list->base[i];
      entity2->group->entityCount --;
      EntityGroup_Release(entity2->group);
      entity2->group = group1;
      group1->entityCount ++;
      EntityGroup_Take(entity2->group);
      (void)PositionAdd(&entity2->offset, &positionDiff);
   }

   // Step 2: Shift to front of destination list
   EntityListShift(list, index2Start, index1Start, size2, NULL);


   // Step 3: Check for anything that is still holding onto group2
   if(group2->refCount > 1)
   {
      size_t i;
      for(i = 0; i < list->count; i++)
      {
         struct entity * entity = list->base[i];
         if(Entity_IsRotator(entity))
         {
            DECLCAST(struct entityRotator, rotator, entity);
            if(rotator->lastRotated == group2)
            {
               EntityRotator_SetLastRotated(rotator, group1);
            }
         } 
      }
   }
   EntityGroup_Release(group2);

}

bool BlockEngine_BlockJoin(int x1, int y1, int z1, int x2, int y2, int z2)
{
   size_t index1, index2;
   struct position p1, p2;
   struct entity * entity1 = EntityListFindByPosition(&PlayList, 
                                                      PositionSet(&p1, x1, y1, z1), 
                                                      &index1);
   struct entity * entity2 = EntityListFindByPosition(&PlayList, 
                                                      PositionSet(&p2, x2, y2, z2), 
                                                      &index2);

   if(entity1 == NULL || entity2 == NULL)
   {
      return false;
   }
   if(entity1->group == entity2->group)
   {
      return true;
   }

   index1 = EntityListFindFirstInGroup(&PlayList, index1);
   index2 = EntityListFindFirstInGroup(&PlayList, index2);
   
   EntityJoin(&PlayList, index1, index2);
   return true;
}


const struct blockType * BlockEngine_GetBlockAt(int x, int y, int z, 
                                               enum blockFacing * facing)
{
   struct entity * entity = FindBlock(&PlayList, NULL, NULL, x, y, z, 
                                      eEPT_Current, eEPT_Current, NULL);
   if(entity == NULL)
   {
      return NULL;
   }
   if(facing != NULL)
   {
      (*facing) = entity->facing;
   }
   return entity->type;
}

static inline
bool EntityIsNextTo(const struct entity * a, 
                    const struct entity * b)
{
   struct position diff;
   unsigned int length;
   PositionCopy(&diff, &a->offset);
   PositionSubtract(&diff, &b->offset);
   length = PositionManhattanLength(&diff);
   return length == 1;
}


static inline
bool IsNextToList(const struct entityList * list,
                  size_t first, size_t last, size_t index)
{
   size_t i;
   const struct entity * entity = list->base[index];
   for(i = first; i <= last; i++)
   {
      if(EntityIsNextTo(entity, list->base[i]))
      {
         return true;
      }
   }
   return false;
}

static inline
void RegroupEntities(struct entityList * list,
                     const size_t firstIndex,
                     const size_t lastIndex,
                     const size_t breaks[6])
{
   size_t i;
   size_t nextBreaksIndex = 0;
   struct entityGroup * group = NULL;
   struct position offset;
   for(i = firstIndex; i <= lastIndex; i++)
   {
      struct entity * entity = list->base[i];

      if(i == breaks[nextBreaksIndex])
      {
         nextBreaksIndex ++;
         // Create the group
         group = EntityGroup_Create();

         // Set group position and next to the current block
         Entity_GetPositionCurrent(entity, &group->position);
         PositionCopy(&group->next, &group->position);
         
         // Compute the offset for the group change
         PositionCopy(&offset, &entity->group->position);
         PositionSubtract(&offset, &group->position);
         
         // Release and set the group.
         // Dont take because we already have it      
         EntityGroup_Release(entity->group);
         entity->group = group;
      }
      else
      {
         // Release and set the group.
         EntityGroup_Release(entity->group);
         entity->group = group;
         EntityGroup_Take(entity->group);
      }
      // Add the offset to the entity position
      PositionAdd(&entity->offset, &offset);
      
      group->entityCount ++;

      if(entity->immovable)
      {
         group->immovable = true;
      }
   }
}

static inline
void BreakUnconnectedGroup(struct entityList * list, 
                           const struct entityGroup * group)
{
   const size_t firstIndex = EntityListFindFirstInGroup2(list, group);
   const size_t lastIndex  = EntityListFindLastInGroup(list, firstIndex);
   size_t breaks[6] = { 
      list->count, list->count, 
      list->count, list->count, 
      list->count, list->count
   };
   size_t breaksIndex = 0;
   size_t breakStart = firstIndex;

   // Find and sort all the entities in connected groups
   for(breaksIndex = 0; 
       breaksIndex < 6 && breakStart <= lastIndex; 
       breaksIndex ++)
   {
      size_t checkIndex = breakStart + 1;
      size_t lastCheckIndex = lastIndex + 1;

      while(lastCheckIndex > checkIndex)
      {
         if(IsNextToList(list, breakStart, checkIndex - 1, checkIndex))
         {
            checkIndex ++;
            lastCheckIndex = lastIndex + 1;
         }
         else
         {
            struct entity * temp;
            lastCheckIndex --;
            EntityListShift(list, checkIndex, lastCheckIndex, 1, &temp);
         }
      }
      breaks[breaksIndex] = breakStart;
      breakStart = checkIndex;
   }

   // Regroup all the entities

   RegroupEntities(list, firstIndex, lastIndex, breaks);
   
}


static inline
void BlockEngine_RemoveEntity(struct entityList * list, struct entity * entity)
{
   struct entityGroup * group = entity->group;
   EntityGroup_Take(group);        
   EntityRemove(list, entity);
   
   if(group->entityCount > 0)
   {
      BreakUnconnectedGroup(list, group);
   }
   EntityGroup_Release(group);
}

bool BlockEngine_BlockRemove(int x, int y, int z)
{
   struct entity * entity;
   entity = FindBlock(&PlayList, NULL, NULL, x, y, z,
                      eEPT_Current, eEPT_Current, NULL);
   if(entity == NULL)
   {
      return false;
   }

   BlockEngine_RemoveEntity(&PlayList, entity);

   return true;
}

static inline
void Entity_SetImmovable(const struct entityList * list, struct entity * entity, 
                         bool immovable)
{
   entity->immovable = immovable;
   if(immovable)
   {
      entity->group->immovable = true;
   }
   else
   {
      size_t firstIndex = EntityListFindFirstInGroup2(list, entity->group);
      size_t lastIndex  = EntityListFindLastInGroup(list, firstIndex);
      size_t i;

      entity->group->immovable = false;
      for(i = firstIndex; i <= lastIndex; i++)
      {
         if(list->base[i]->immovable)
         {
            entity->group->immovable = true;
            break;
         }
      }
   }
}

bool BlockEngine_BlockSetImmovable(int x, int y, int z, bool immovable)
{
   struct entity * entity;
   
   entity = FindBlock(&PlayList, NULL, NULL, x, y, z,
                      eEPT_Current, eEPT_Current, NULL);
   if(entity == NULL)
   {
      return false;
   }
   Entity_SetImmovable(&PlayList, entity, immovable);
   return true;
}

static const enum blockFacing FacingRotationTable[] = {
// eBF_North, eBF_East, eBF_South, eBF_West,
   eBF_East, eBF_South, eBF_West, eBF_North, // eBR_Right
   eBF_West, eBF_North, eBF_East, eBF_South  // eBR_Left
};

static inline
enum blockFacing GetRotatedBlockFacing(enum blockFacing facing, 
                                       enum blockRotation rotation)
{
   return FacingRotationTable[facing + (rotation * 4)];
}

static inline
void EntityDrawRotation(struct entity * entity)
{
   struct position pos;
   Entity_GetPositionCurrent(entity, &pos);
   UIEngine_Entity_Move(entity->gfx, pos.x, pos.y, pos.z);
   UIEngine_Entity_Face(entity->gfx, entity->facing);
}

static inline
bool BlockEngine_RotateGroup(struct entityList * list, size_t index, 
                              enum blockRotation rotation)
{
   struct entity * pivot = list->base[index];
   size_t startIndex = EntityListFindFirstInGroup(list, index);
   size_t endIndex   = EntityListFindLastInGroup(list, index);
   size_t i;
   
   // Check to see if moving is even possible
   for(i = startIndex; i <= endIndex; i ++)
   {
      struct entity * entity = list->base[i];
      if(!Entity_CanMove(entity))
      {
         return false;
      }
   }

   
   // Do the rotation
   for(i = startIndex; i <= endIndex; i ++)
   {
      enum blockFacing rotFacing = (rotation == eBR_Right) ? eBF_East : eBF_West;
      struct position fromPivot;
      struct entity * entity = list->base[i];
      entity->facing = GetRotatedBlockFacing(entity->facing, rotation);
      
      // Compute position realtive to the pivot
      PositionCopy(&fromPivot,     &entity->offset);
      PositionSubtract(&fromPivot, &pivot->offset);

      // Rotate
      PositionRotate(&fromPivot, rotFacing);

      // Reposition the rotation
      PositionAdd(&fromPivot, &pivot->offset);

      // Copy over the offest
      PositionCopy(&entity->offset, &fromPivot);
      
      EntityDrawRotation(entity);
   }
   return true;
}

bool BlockEngine_Rotate(int x, int y, int z, enum blockRotation rotation)
{
   size_t index;
   struct entity * entity = FindBlock(&PlayList, NULL, NULL, x, y, z,
                            eEPT_Current, eEPT_Current, &index);
   if(entity == NULL)
   {
      return false;
   }
   (void)BlockEngine_RotateGroup(&PlayList, index, rotation);

   return true;
}


size_t BlockEngine_GetBlockCount(void)
{
   return PlayList.count;
}

size_t BlockEngine_WorldSerialize(struct serializedBlock * blocks, size_t size)
{
   struct entityGroup * lastGroup = NULL;
   size_t i;
   uint32_t nextGroupIndex = 0;
   uint32_t currentGroupIndex = 0;
   
   if(blocks == NULL || size < PlayList.count)
   {
      return PlayList.count;
   }
   
   for(i = 0; i < PlayList.count; i++)
   {
      const struct entity * entity = PlayList.base[i];
      struct serializedBlock * sBlock = &blocks[i];
      struct position position;
      Entity_GetPositionCurrent(entity, &position);
      if(entity->group != lastGroup)
      {
         currentGroupIndex = nextGroupIndex;
         nextGroupIndex ++;
         lastGroup = entity->group;
      }

      sBlock->x          = position.x;
      sBlock->y          = position.y;
      sBlock->z          = position.z;
      sBlock->immovable  = entity->immovable;
      sBlock->groupIndex = currentGroupIndex;
      sBlock->type       = entity->type;
      sBlock->facing     = entity->facing; // Maybe don't rely on enum
      sBlock->flags      = BLOCKENGINE_FLAG_NONE;

      if(Entity_IsRotator(entity)) 
      {
         DECLCAST(struct entityRotator, rotator, entity);
         if(rotator->lastRotated != NULL)
         {
            const struct entity * above = FindBlock(&PlayList, 
                                                    rotator->parent.group, 
                                                    &rotator->parent, 
                                                    0, 0, 1,
                                                    eEPT_Current, eEPT_Current,
                                                    NULL);
            if(above != NULL && above->group == rotator->lastRotated)
            {
               sBlock->flags |= BLOCKENGINE_FLAG_ROTATOR_DONE;
            }
         }
      }
   }
   return PlayList.count; 
}

void BlockEngine_WorldDeserialize(const struct serializedBlock * blocks, size_t size)
{
   size_t i;
   uint32_t lastGroup = 0xFFFFFFFF;
   struct entityGroup * group = NULL;
   // Get an address of something that isn't NULL
   struct entityGroup * lastRotateMarker = (struct entityGroup *)&i;

   BlockEngine_Clear();

   for(i = 0; i < size; i++)
   {
      const struct serializedBlock * sBlock = &blocks[i];
      struct entity * entity;
      struct position pos;
      if(sBlock->groupIndex != lastGroup)
      {
         if(group != NULL)
         {
            EntityGroup_Release(group);
         }
         group = EntityGroup_Create();
         PositionSet(&group->position, sBlock->x, sBlock->y, sBlock->z);
         PositionCopy(&group->next, &group->position);       
         lastGroup = sBlock->groupIndex;
      }

      entity = Entity_New(sBlock->type, group);
      PositionSet(&entity->offset, sBlock->x - group->position.x,
                                   sBlock->y - group->position.y,
                                   sBlock->z - group->position.z);
      entity->immovable = sBlock->immovable;
      if(entity->immovable)
      {
         group->immovable = true;
      }
      EntityListAdd(&PlayList, entity);
      Entity_GetPositionCurrent(entity, &pos);
      entity->facing = sBlock->facing;
      UIEngine_Entity_Move(entity->gfx, pos.x, pos.y, pos.z);
      UIEngine_Entity_Face(entity->gfx, entity->facing);

      if((sBlock->flags & BLOCKENGINE_FLAG_ROTATOR_DONE) == 
         BLOCKENGINE_FLAG_ROTATOR_DONE && 
         Entity_IsRotator(entity))
      {
         DECLCAST(struct entityRotator, rotator, entity);
         rotator->lastRotated = lastRotateMarker;
      }
      
   }
   if(group != NULL)
   {
      EntityGroup_Release(group);
   }

   // Update Rotators
   for(i = 0; i < PlayList.count; i++)
   {
      struct entity * entity = PlayList.base[i];
      if(Entity_IsRotator(entity))
      {
         DECLCAST(struct entityRotator, rotator, entity);
         if(rotator->lastRotated == lastRotateMarker)
         {
            const struct entity * above = FindBlock(&PlayList, 
                                                    rotator->parent.group, 
                                                    &rotator->parent, 
                                                    0, 0, 1,
                                                    eEPT_Current,
                                                    eEPT_Current,
                                                    NULL);
            
            // So we don't try to "Release" on change.
            rotator->lastRotated = NULL; 

            if(above != NULL)
            {
               EntityRotator_SetLastRotated(rotator, above->group);
            }
         }
      }
   }
}


