#include "UIEngine.h"
#include "GameData.h"
#include "Position.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>


// Not suggested, but might as well as it doesn't hurt.
#define KEY_ESC 27




#define COLOR_PAIR_TEXT        1
#define COLOR_PAIR_UNKNOWN     2
#define COLOR_PAIR_CONVEYOR    3
#define COLOR_PAIR_ROTATOR     4
#define COLOR_PAIR_WIRE        5
#define COLOR_PAIR_SENSOR      6
#define COLOR_PAIR_PUSHER      7
#define COLOR_PAIR_GRINDER     8
#define COLOR_PAIR_GRASS       9
#define COLOR_PAIR_WOOD        10
#define COLOR_PAIR_DUPLICATOR  11
#define COLOR_PAIR_STEELFRAME  12
#define COLOR_PAIR_WELDER      13
#define COLOR_PAIR_WELDERBEAM  14

#define MAX_DT  250

#define BLINK_TIMEOUT_MS 500





struct appearance
{
   int color;
   chtype character;
   bool facingMatters;
};

struct uiBlockType
{
   const struct blockType * type;
   const char * name;
   struct appearance appearance;
};

static struct uiBlockType UIBlockTypes[] = {
   { NULL, "conveyor",    { COLOR_PAIR_CONVEYOR,   '=', true  } },
   { NULL, "rotator-cw",  { COLOR_PAIR_ROTATOR,    'r', false } },
   { NULL, "rotator-ccw", { COLOR_PAIR_ROTATOR,    '7', false } },
   { NULL, "wire",        { COLOR_PAIR_SENSOR,     '+', false } },
   { NULL, "sensor",      { COLOR_PAIR_WIRE,       '$', true  } },
   { NULL, "pusher",      { COLOR_PAIR_PUSHER,     'P', true  } },
   { NULL, "grinder",     { COLOR_PAIR_GRINDER,    'G', true  } },
   { NULL, "welder",      { COLOR_PAIR_WELDER,     'W', true  } },
   { NULL, "wood",        { COLOR_PAIR_WOOD,       '#', false } },
   { NULL, "grass",       { COLOR_PAIR_GRASS,      '~', false } },
   { NULL, "duplicator",  { COLOR_PAIR_DUPLICATOR, '8', false } },
   { NULL, "steel-frame", { COLOR_PAIR_STEELFRAME, '.', false } },
};

struct uiSpecificBlockTypes
{
   const struct uiBlockType * grinder;
   const struct uiBlockType * welder;
   const struct uiBlockType * pusher;
   const struct uiBlockType * wire;
   const struct uiBlockType * sensor;
};

static struct uiSpecificBlockTypes UISBT;

static const struct appearance AppearanceUnknown = {
   COLOR_PAIR_UNKNOWN, '?', false
};



static const size_t UIBlockTypesCount = sizeof(UIBlockTypes) / sizeof(struct uiBlockType);

static inline
const struct appearance * GetAppearanceFromType(const struct blockType * type)
{
   size_t i;
   for(i = 0; i < UIBlockTypesCount; i++)
   {
      if(type == UIBlockTypes[i].type)
      {
         return &UIBlockTypes[i].appearance;
      }
   }
   return &AppearanceUnknown;
}

static inline
struct uiBlockType * UIBlockTypesFind(const char * name, size_t * index)
{
   size_t i;
   for(i = 0; i < UIBlockTypesCount; i++)
   {
      struct uiBlockType * type = &UIBlockTypes[i];
      if(strcmp(type->name, name) == 0)
      {
         if(index != NULL)
         {
            (*index) = i;
         }
         return type;
      }
   }
   return NULL;
}

static inline
void UIBlockTypesPopulate(void)
{
   size_t i;
   for(i = 0; i < UIBlockTypesCount; i++)
   {
      struct uiBlockType * type = &UIBlockTypes[i];
      type->type = GameData_GetTypeByName(type->name);
   }

   // Lookup specific types
   UISBT.grinder = UIBlockTypesFind("grinder", NULL);
   UISBT.welder  = UIBlockTypesFind("welder",  NULL);
   UISBT.sensor  = UIBlockTypesFind("sensor",  NULL);
   UISBT.wire    = UIBlockTypesFind("wire",    NULL);
   UISBT.pusher  = UIBlockTypesFind("pusher",  NULL);
}

struct uiEngineEntity
{
   const struct uiBlockType * type;
   struct position position;
   enum blockFacing facing;
   unsigned int frame;
};

enum symbolShow
{
   eSS_Appearance,
   eSS_Facing
};

static bool Running;
static int MaxX, MaxY;

static struct position Cursor;

#define ENTITYLIST_GROWBY 32
static struct uiEngineEntity ** EntityList = NULL;
static size_t EntityListSize = 0;
static size_t EntityListCount = 0;

static chtype FacingCharacters[4];


struct selected
{
   const struct blockType * type;
   const struct appearance * appearance;
   enum blockFacing facing;
};

static struct selected Selected;


static inline
bool IsTypeName(const struct blockType * type, const char * name)
{
   return strcmp(type->name, name) == 0;
}


static inline
void ColorSetup(void)
{
   start_color();

   init_pair(COLOR_PAIR_TEXT,       COLOR_WHITE,  COLOR_BLACK);
   init_pair(COLOR_PAIR_UNKNOWN,    COLOR_WHITE,  COLOR_RED);
   init_pair(COLOR_PAIR_CONVEYOR,   COLOR_YELLOW, COLOR_BLACK);
   init_pair(COLOR_PAIR_ROTATOR,    COLOR_BLUE,   COLOR_BLACK);
   init_pair(COLOR_PAIR_WIRE,       COLOR_WHITE,  COLOR_BLACK);
   init_pair(COLOR_PAIR_SENSOR,     COLOR_WHITE,  COLOR_BLACK);
   init_pair(COLOR_PAIR_PUSHER,     COLOR_WHITE,  COLOR_BLACK);
   init_pair(COLOR_PAIR_GRINDER,    COLOR_RED,    COLOR_BLACK);
   init_pair(COLOR_PAIR_GRASS,      COLOR_GREEN,  COLOR_BLACK);
   init_pair(COLOR_PAIR_WOOD,       COLOR_RED,    COLOR_BLACK);
   init_pair(COLOR_PAIR_DUPLICATOR, COLOR_BLACK,  COLOR_BLUE);
   init_pair(COLOR_PAIR_STEELFRAME, COLOR_WHITE,  COLOR_BLACK);
   init_pair(COLOR_PAIR_WELDER,     COLOR_CYAN,   COLOR_BLACK);
   init_pair(COLOR_PAIR_WELDERBEAM, COLOR_YELLOW, COLOR_BLACK);
}


static inline
void SelectionInit(void)
{
   Selected.type       = &GameData_GetAllTypes(NULL)[0]; 
   Selected.appearance = GetAppearanceFromType(Selected.type);
   Selected.facing     = eBF_North;
}

static inline
void SelectionNextType(void)
{
   unsigned int count;
   const struct blockType * types = GameData_GetAllTypes(&count);
   unsigned int index = GameData_GetIndex(Selected.type);
   index ++;
   if(index >= count)
   {
      index = 0;
   }
   Selected.type = &types[index];
   Selected.appearance = GetAppearanceFromType(Selected.type);
   Selected.facing = eBF_North;
}

static inline
void SelectionPreviousType(void)
{
   unsigned int count;
   const struct blockType * types = GameData_GetAllTypes(&count);
   unsigned int index = GameData_GetIndex(Selected.type);
   if(index == 0)
   {
      index = count -1;
   }
   else
   {
      index --;
   }
   Selected.type = &types[index];
   Selected.appearance = GetAppearanceFromType(Selected.type);
   Selected.facing = eBF_West;
}

static inline
void SelectionNext(void)
{
   if(Selected.appearance->facingMatters)
   {
      switch(Selected.facing)
      {
      case eBF_North:
         Selected.facing = eBF_East;
         break;
      case eBF_East:
         Selected.facing = eBF_South;
         break;
      case eBF_South:
         Selected.facing = eBF_West;
         break;
      case eBF_West:
         SelectionNextType();
         break;
      }
   }
   else
   {
      SelectionNextType();
   }
}

static inline
void SelectionPrevious(void)
{
   if(Selected.appearance->facingMatters)
   {
      switch(Selected.facing)
      {
      case eBF_West:
         Selected.facing = eBF_South;
         break;
      case eBF_South:
         Selected.facing = eBF_East;
         break;
      case eBF_East:
         Selected.facing = eBF_North;
         break;
      case eBF_North:
         SelectionPreviousType();
         break;
      }
   }
   else
   {
      SelectionPreviousType();
   }
}
static inline
void SelectionSet(const struct blockType * type, enum blockFacing facing)
{
   Selected.type = type;
   Selected.appearance = GetAppearanceFromType(Selected.type);
   if(Selected.appearance->facingMatters)
   {
      Selected.facing = facing;
   }
   else
   {
      Selected.facing = eBF_North;
   }
}

static inline
void FacingCharactersSetup(void)
{
   FacingCharacters[eBF_North] = ACS_UARROW;
   FacingCharacters[eBF_East]  = ACS_RARROW;
   FacingCharacters[eBF_South] = ACS_DARROW;
   FacingCharacters[eBF_West]  = ACS_LARROW;
}


static inline
void EntityListSetup(void)
{
   EntityListSize = ENTITYLIST_GROWBY;
   EntityListCount = 0;
   EntityList = malloc(sizeof(struct uiEngineEntity *) * EntityListSize);
}

static inline
void EntityListDestroy(void)
{
   free(EntityList);
   EntityList = NULL;
   EntityListSize = 0;
   EntityListCount = 0;
}

static inline
void EntityListAdd(struct uiEngineEntity * entity)
{
   size_t newIndex;
   if(EntityListCount >= EntityListSize)
   {
      EntityListSize += ENTITYLIST_GROWBY;
      EntityList = realloc(EntityList, 
                           sizeof(struct uiEngineEntity*) * EntityListSize); 
   }
   newIndex = EntityListCount;
   EntityListCount ++;
   EntityList[newIndex] = entity;
}


static inline
bool EntityListFind(const struct uiEngineEntity * entity, size_t * index)
{
   size_t i;
   for(i = 0; i < EntityListCount; i++)
   {
      if(EntityList[i] == entity)
      {
         (*index) = i;
         return true;
      }
   }
   return false;
}

static inline
void EntityListRemove(struct uiEngineEntity * entity)
{
   size_t index;
   if(EntityListFind(entity, &index))
   {
      EntityListCount --;
      if(index != EntityListCount)
      {
         EntityList[index] = EntityList[EntityListCount];
      }
   }
}

bool UIEngine_Setup(void)
{
   initscr();
   if(has_colors() == FALSE)
   {
      endwin();
      printf("Terminal Colors are not supported!");
      return true;
   }

   // Setup State
   noecho();
   cbreak();
   keypad(stdscr, TRUE);
   timeout(MAX_DT);
   getmaxyx(stdscr, MaxY, MaxX);
 
   UIBlockTypesPopulate(); 
   EntityListSetup(); 
   ColorSetup();
   FacingCharactersSetup();

   SelectionInit();

   Running = true;
   Cursor.x = 0;
   Cursor.y = 0;
   Cursor.z = 0;
   return false;
}

void UIEngine_Teardown(void)
{
   EntityListDestroy();
   endwin();
   //printf("Colors: %d\n", COLORS);
}

bool UIEngine_IsRunning(void)
{
   return Running;
}


static inline
void RenderAppearanceChar(int depth, enum blockFacing facing,
                          const struct appearance * appearance, 
                          enum symbolShow show)
{
   chtype flags = 0;
   chtype symbol = ' ';

   switch(depth)
   {
   case 0:
      flags = A_BOLD;
      break;
   case 2:
      flags = A_DIM;
      break;
   }

   switch(show)
   {
   case eSS_Appearance:
      symbol = appearance->character;
      break;
   case eSS_Facing:
      if(appearance->facingMatters)
      {
         symbol = FacingCharacters[facing];
      }
      else
      {
         symbol = appearance->character;
      }
      break;
   }

   attron(COLOR_PAIR(appearance->color));
   addch(symbol | flags);
   attroff(COLOR_PAIR(appearance->color));
}

static inline
void RenderEntityAt(int x, int y, int depth, 
                    const struct uiEngineEntity * entity,
                    enum symbolShow show)
{
   const struct appearance * appearance;
   if(entity->type == NULL)
   {
      appearance = &AppearanceUnknown; 
   }
   else
   {
      appearance = &entity->type->appearance;
   }
   move(y, x);
   RenderAppearanceChar(depth, entity->facing, appearance, show);
}

static inline
bool PositionWithinScreen(const struct position * position)
{
   return position->x >= 0 && position->x < MaxX &&
          position->y >= 0 && position->y < MaxY;
}

static inline
bool EntityWithinScreen(const struct uiEngineEntity * entity)
{
   return PositionWithinScreen(&entity->position);
}

static inline
void Entity_PositionOffsetRotate(const struct uiEngineEntity * entity,
                                 struct position * position,
                                 int x, int y, int z)

{
   PositionSet(position, x, y, z);
   PositionRotate(position, entity->facing);
   PositionAdd(position, &entity->position);
}

static inline
void RenderGrinderEffect(struct uiEngineEntity * entity, int depth)
{
   struct position pos;
   chtype frames[2] = { '+', 'x' };
   struct appearance appearance = {
      COLOR_PAIR_GRINDER, frames[entity->frame], false 
   };


   Entity_PositionOffsetRotate(entity, &pos, 0, -1, 0);

   if(PositionWithinScreen(&pos))
   {
      move(pos.y, pos.x);
      RenderAppearanceChar(depth, eBF_North,
                           &appearance, 
                           eSS_Appearance);
   
   }

   entity->frame ++;
   if(entity->frame > 1)
   {
      entity->frame = 0;
   }
}

static inline
void RenderWelderEffect(size_t welderIndex, int depth)
{
   struct position pos;
   struct uiEngineEntity * entity = EntityList[welderIndex];
   
   Entity_PositionOffsetRotate(entity, &pos, 0, -1, 0);
   if(PositionWithinScreen(&pos))
   {
      size_t i;
      int flags = 0; 
      const chtype symbols[] = {
         ACS_BULLET,   // 0000
         '-',          // 0001
         '-',          // 0010
         ACS_HLINE,    // 0011
         'i',          // 0100
         ACS_ULCORNER, // 0101
         ACS_URCORNER, // 0110
         ACS_TTEE,     // 0111
         'i',          // 1000
         ACS_LLCORNER, // 1001
         ACS_LRCORNER, // 1010
         ACS_BTEE,     // 1011
         ACS_VLINE,    // 1100
         ACS_LTEE,     // 1101
         ACS_RTEE,     // 1110
         ACS_PLUS,     // 1111
      };
      struct appearance appearance;
      for(i = 0; i < EntityListCount; i++)
      {
         struct uiEngineEntity * otherEntity = EntityList[i];
         if(otherEntity->position.z == Cursor.z - depth && 
            otherEntity->type == UISBT.welder)
         {
            struct position otherPos, diff;
            Entity_PositionOffsetRotate(otherEntity, &otherPos, 0, -1, 0);
            if(PositionIsEqual(&otherPos, &pos))
            {
               // If we are the welder index then attach to the beam to ourself
               PositionCopy(&otherPos, &otherEntity->position);
            }
            PositionCopy(&diff, &otherPos);
            PositionSubtract(&diff, &pos);
            if(diff.x == 1 && diff.y == 0)
            {
               flags |= 0x01;
            }
            else if(diff.x == -1 && diff.y == 0)
            {
               flags |= 0x02;
            }
            else if(diff.x == 0 && diff.y == 1)
            {
               flags |= 0x04;
            }
            else if(diff.x == 0 && diff.y == -1)
            {
               flags |= 0x08;
            }
         }
      }
      appearance.character = symbols[flags];
      appearance.color = COLOR_PAIR_WELDERBEAM;
      appearance.facingMatters = false;
      move(pos.y, pos.x);
      RenderAppearanceChar(depth, eBF_North, &appearance, eSS_Appearance);
   }
}

static inline 
void RenderEntities(enum symbolShow show)
{
   size_t i;
   int depth;
   for(depth = 2; depth >= 0; depth-- )
   {
      // Render Pre-Effects
      for(i = 0; i < EntityListCount; i++)
      {
         struct uiEngineEntity * entity = EntityList[i];

         if(entity->position.z == Cursor.z - depth)
         {
            if(entity->type == UISBT.welder)
            {
               RenderWelderEffect(i, depth);
            }
         }
      }
      // Render Blocks
      for(i = 0; i < EntityListCount; i++)
      {
         const struct uiEngineEntity * entity = EntityList[i];

         if(EntityWithinScreen(entity) &&
            entity->position.z == Cursor.z - depth)
         {
            RenderEntityAt(entity->position.x, entity->position.y, depth, 
                           entity, show);
         }
      }

      // Render Post-Effects
      for(i = 0; i < EntityListCount; i++)
      {
         struct uiEngineEntity * entity = EntityList[i];

         if(entity->position.z == Cursor.z - depth)
         {
            if(entity->type == UISBT.grinder)
            {
               RenderGrinderEffect(entity, depth);           
            }
         }
      }
   }
}

static inline
const char * FacingToString(enum blockFacing facing)
{
   const char * string;
   switch(facing)
   {
   case eBF_North:
      string = "North";
      break;
   case eBF_East:
      string = "East";
      break;
   case eBF_South:
      string = "South";
      break;
   case eBF_West:
      string = "West";
      break;
   default:
      string = "?";
      break;
   }
   return string;
}

static inline
void RenderSelectedItem(int x, int y, enum symbolShow show)
{
   move(y, x);
   printw("Selected Item: ");
   RenderAppearanceChar(0, Selected.facing, Selected.appearance, show);
   
   printw(" %s", Selected.type->name);
   if(Selected.appearance->facingMatters)
   {
      printw(" :: %s", FacingToString(Selected.facing));
   }
}

static inline
void RenderMode(int x, int y)
{
   const char * modeStr = "?";
   enum mode mode = GameEngine_GetMode();
   switch(mode)
   {
   case eM_Edit:    modeStr = "Edit";    break;
   case eM_Playing: modeStr = "Playing"; break;
   case eM_Paused:  modeStr = "Paused";  break;
      
   }
   move(y, x);
   printw("Mode: %s", modeStr);
}

static inline
void RenderCursorLocation(int x, int y)
{
   move(y, x);
   printw("Cursor: (%d, %d, %d)", Cursor.x, Cursor.y, Cursor.z);
}


static inline
void RenderUI(enum symbolShow show)
{
   RenderMode(0,                   MaxY - 2);
   RenderSelectedItem(0,           MaxY - 1, show);
   RenderCursorLocation(MaxX - 25, MaxY - 1);
}

static inline
void UIEngine_HandleInput(int ch)
{
   switch(ch)
   {
   case 'q':
   case KEY_ESC:
      Running = false;
      break;
   case KEY_UP:
      Cursor.y = Cursor.y - 1;
      if(Cursor.y < 0) Cursor.y = 0;
      break;
   case KEY_DOWN:
      Cursor.y = Cursor.y + 1;
      if(Cursor.y >= MaxY) Cursor.y = MaxY - 1;
      break;
   case KEY_LEFT:
      Cursor.x = Cursor.x - 1;
      if(Cursor.x < 0) Cursor.x = 0;
      break;
   case KEY_RIGHT:
      Cursor.x = Cursor.x + 1;
      if(Cursor.x >= MaxX) Cursor.x = MaxX - 1;
      break;
   case '>':
      Cursor.z = Cursor.z - 1;
      break;
   case '<':
      Cursor.z = Cursor.z + 1;
      break;
   case 'd':
      SelectionNext();
      break;
   case 'a':
      SelectionPrevious();
      break;
   case 'w':
      GameEngine_BlockAdd(Selected.type, Cursor.x, Cursor.y, Cursor.z,
                          Selected.facing);
      break;
   case 'e':
      {
         enum blockFacing facing;
         const struct blockType * type = GameEngine_GetBlockAt(Cursor.x, 
                                                               Cursor.y, 
                                                               Cursor.z, 
                                                               &facing);
         if(type != NULL)
         {
            SelectionSet(type, facing);
         }
      }
      break;
   case 's':
      GameEngine_BlockRemove(Cursor.x, Cursor.y, Cursor.z);
      break;
   case ' ':
      {
         enum mode mode = GameEngine_GetMode();
         switch(mode)
         {
         case eM_Edit:
            mode = eM_Playing;
            break;
         case eM_Playing:
            mode = eM_Edit;
            break;
         case eM_Paused:
            mode = eM_Playing;
            break;
         }
         GameEngine_SetMode(mode);
      }
      break;
   }
}

static inline
enum symbolShow UIEngine_GetSymbolShow(unsigned int dt_ms)
{
   static unsigned int timer_ms = BLINK_TIMEOUT_MS;
   static enum symbolShow show = eSS_Appearance;
  
   if(timer_ms <= dt_ms)
   {
      timer_ms = BLINK_TIMEOUT_MS;
      switch(show)
      {
      case eSS_Appearance:
         show = eSS_Facing;
         break;
      case eSS_Facing:
         show = eSS_Appearance;
         break;
      }
   } 
   else
   {
      timer_ms -= dt_ms;
   }
   return show; 
}

void UIEngine_Step(void)
{

   int ch;
   enum symbolShow show = UIEngine_GetSymbolShow(MAX_DT);

   clear();

   RenderEntities(show);
   RenderUI(show);

   move(Cursor.y, Cursor.x);


   refresh();
   
   ch = getch();
   if(ch != ERR)
   {
      UIEngine_HandleInput(ch);
      GameEngine_Step();
   }

}

const struct blockType * UIEngine_GetSelectedBlockType(void)
{
   return Selected.type;
}


const struct uiBlockType * GetUIBlockTypeFromType(const struct blockType * type)
{
   size_t i;
   for(i = 0; i < UIBlockTypesCount; i++)
   {
      if(UIBlockTypes[i].type == type)
      {
         return &UIBlockTypes[i];
      }
   }
   return NULL;
}


struct uiEngineEntity * UIEngine_Entity_Create(const struct blockType * type)
{
   struct uiEngineEntity * entity = malloc(sizeof(struct uiEngineEntity));
   PositionSet(&entity->position, 0, 0, 0);
   entity->type = GetUIBlockTypeFromType(type);
   entity->frame = 0;

   EntityListAdd(entity);

   return entity;
}

void UIEngine_Entity_Destroy(struct uiEngineEntity * entity)
{
   EntityListRemove(entity);
   free(entity);
}

void UIEngine_Entity_Move(struct uiEngineEntity * entity, int x, int y, int z)
{
   PositionSet(&entity->position, x, y, z);
}

void UIEngine_Entity_Face(struct uiEngineEntity * entity, 
                          enum blockFacing facing)
{
   entity->facing = facing;
}

void UIEngine_Entity_Activate(struct uiEngineEntity * entity, bool isActive)
{
}

const struct blockType * UIEngine_Entity_GetType(struct uiEngineEntity * entity)
{
   return entity->type->type;
}



