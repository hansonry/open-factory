#include <string.h>
#include "GameData.h"

#define BLOCKFLAGS_ALLSTICKY { BLOCKFLAGS_STICKY, \
                               BLOCKFLAGS_STICKY, \
                               BLOCKFLAGS_STICKY, \
                               BLOCKFLAGS_STICKY, \
                               BLOCKFLAGS_STICKY, \
                               BLOCKFLAGS_STICKY }

#define BLOCKFLAGS_TOPNOTSTICKY { BLOCKFLAGS_STICKY, \
                                  BLOCKFLAGS_STICKY, \
                                  BLOCKFLAGS_STICKY, \
                                  BLOCKFLAGS_STICKY, \
                                  BLOCKFLAGS_NONE,   \
                                  BLOCKFLAGS_STICKY }

#define BLOCKFLAGS_NORTHNOTSTICKY { BLOCKFLAGS_STICKY, \
                                    BLOCKFLAGS_STICKY, \
                                    BLOCKFLAGS_STICKY, \
                                    BLOCKFLAGS_NONE,   \
                                    BLOCKFLAGS_STICKY, \
                                    BLOCKFLAGS_STICKY }

#define BLOCKFLAGS_TOPBOTTOMNOTSTICKY { BLOCKFLAGS_STICKY, \
                                        BLOCKFLAGS_STICKY, \
                                        BLOCKFLAGS_STICKY, \
                                        BLOCKFLAGS_STICKY, \
                                        BLOCKFLAGS_NONE,   \
                                        BLOCKFLAGS_NONE }


static const struct blockType BlockTypes[] = {
   { "conveyor",       false, BLOCKFLAGS_TOPNOTSTICKY       },
   { "rotator-cw",     false, BLOCKFLAGS_TOPNOTSTICKY       },
   { "rotator-ccw",    false, BLOCKFLAGS_TOPNOTSTICKY       },
   { "wire",           false, BLOCKFLAGS_ALLSTICKY          },
   { "sensor",         false, BLOCKFLAGS_NORTHNOTSTICKY     },
   { "pusher",         false, BLOCKFLAGS_NORTHNOTSTICKY     },
   { "steel-frame",    true,  BLOCKFLAGS_ALLSTICKY          },
   { "grinder",        false, BLOCKFLAGS_NORTHNOTSTICKY     },
   { "welder",         false, BLOCKFLAGS_NORTHNOTSTICKY     },
   { "wood",           false, BLOCKFLAGS_ALLSTICKY          },
   { "grass",          false, BLOCKFLAGS_ALLSTICKY          },
   { "duplicator",     true,  BLOCKFLAGS_TOPBOTTOMNOTSTICKY }
};


static const unsigned int BlockTypesCount = sizeof(BlockTypes) / 
                                            sizeof(struct blockType);

const struct blockType * GameData_GetAllTypes(unsigned int * count)
{
   if(count != NULL)
   {
      (*count) = BlockTypesCount;
   }
   return BlockTypes;
}

const struct blockType * GameData_GetTypeByName(const char * name)
{
   unsigned int i;
   for(i = 0; i < BlockTypesCount; i++)
   {
      if(strcmp(BlockTypes[i].name, name) == 0)
      {
         return &BlockTypes[i];
      }
   }
   return NULL;
}

unsigned int GameData_GetIndex(const struct blockType * type)
{
   return type - BlockTypes;
}

