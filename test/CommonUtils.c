#include <ryanmock.h>
#include "CommonUtils.h"

static int EntityLocation;

const struct blockType BlockTypeNotSticky = {
   "not-sticky", false,
   { BLOCKFLAGS_NONE, BLOCKFLAGS_NONE, 
     BLOCKFLAGS_NONE, BLOCKFLAGS_NONE, 
     BLOCKFLAGS_NONE, BLOCKFLAGS_NONE }
};

const struct blockType BlockTypeSticky = {
   "sticky", false,
   { BLOCKFLAGS_STICKY, BLOCKFLAGS_STICKY, 
     BLOCKFLAGS_STICKY, BLOCKFLAGS_STICKY, 
     BLOCKFLAGS_STICKY, BLOCKFLAGS_STICKY }
};

const struct blockType BlockTypeImmovable = {
   "immovable", true,
   { BLOCKFLAGS_NONE, BLOCKFLAGS_NONE, 
     BLOCKFLAGS_NONE, BLOCKFLAGS_NONE, 
     BLOCKFLAGS_NONE, BLOCKFLAGS_NONE }
};

const struct blockType BlockTypeStickyImmovable = {
   "sticky-immovable", true,
   { BLOCKFLAGS_STICKY, BLOCKFLAGS_STICKY, 
     BLOCKFLAGS_STICKY, BLOCKFLAGS_STICKY, 
     BLOCKFLAGS_STICKY, BLOCKFLAGS_STICKY }
};

const struct blockType BlockTypeGrass = {
   "grass", false,
   { BLOCKFLAGS_NONE, BLOCKFLAGS_NONE, 
     BLOCKFLAGS_NONE, BLOCKFLAGS_NONE, 
     BLOCKFLAGS_NONE, BLOCKFLAGS_NONE }
};

void util_ignore_ui_creation(void)
{
   rmmMockBeginIgnoredAll(UIEngine_Entity_Create);
      rmmExpectAny(type);
   rmmMockEndReturnPtr(&EntityLocation);

   rmmMockBeginIgnoredAll(UIEngine_Entity_Destroy);
      rmmExpectPtrEqual(entity, &EntityLocation);
   rmmMockEnd();
   
   rmmMockBeginIgnoredAll(UIEngine_Entity_Face);
      rmmExpectAny(entity);
      rmmExpectAny(facing);
   rmmMockEnd();
}

void util_ignore_ui_move(void)
{

   rmmMockBeginIgnoredAll(UIEngine_Entity_Move);
      rmmExpectPtrEqual(entity, &EntityLocation);
      rmmExpectAny(x);
      rmmExpectAny(y);
      rmmExpectAny(z);
   rmmMockEnd();
}

#define util_assert_block(expectedType, expectedFacing, x, y, z) \
rmmEnterFunction(util_assert_block);                             \
_util_assert_block(expectedType, expectedFacing, x, y, z);       \
rmmExitFunction(util_assert_block)

void _util_assert_block(const struct blockType * expectedType, 
                        enum blockFacing expectedFacing,
                        int x, int y, int z)
{
   if(expectedType == NULL)
   {
      rmmAssertPtrNULL(BlockEngine_GetBlockAt(x, y, z, NULL));
   }
   else
   {
      enum blockFacing facing;
      rmmAssertPtrEqual(BlockEngine_GetBlockAt(x, y, z, &facing), expectedType);
      rmmAssertEnumEqual(facing, expectedFacing);
   }

   
}

void util_fill_test_blocks(struct commonTestBlockTypes * types)
{
   types->conveyor        = GameData_GetTypeByName("conveyor");
   types->duplicator      = GameData_GetTypeByName("duplicator");
   types->grinder         = GameData_GetTypeByName("grinder");
   types->welder          = GameData_GetTypeByName("welder");
   types->rotatorCW       = GameData_GetTypeByName("rotator-cw");
   types->rotatorCCW      = GameData_GetTypeByName("rotator-ccw");
   types->wire            = GameData_GetTypeByName("wire");
   types->sensor          = GameData_GetTypeByName("sensor");
   types->pusher          = GameData_GetTypeByName("pusher");

   types->immovable       = &BlockTypeImmovable;
   types->notSticky       = &BlockTypeNotSticky;
   types->sticky          = &BlockTypeSticky;
   types->stickyImmovable = &BlockTypeStickyImmovable;
   types->grass           = &BlockTypeGrass;

   rmmAssertPtrNotNULL(types->conveyor);
   rmmAssertPtrNotNULL(types->duplicator);
   rmmAssertPtrNotNULL(types->grinder);
   rmmAssertPtrNotNULL(types->welder);
   rmmAssertPtrNotNULL(types->rotatorCW);
   rmmAssertPtrNotNULL(types->rotatorCCW);
   rmmAssertPtrNotNULL(types->wire);
   rmmAssertPtrNotNULL(types->sensor);
   rmmAssertPtrNotNULL(types->pusher);
}


void util_add_immovable_block(const struct blockType * type, 
                              int x, int y, int z, enum blockFacing facing)
{
   BlockEngine_BlockAdd(type, x, y, z, facing);
   rmmAssertTrue(BlockEngine_BlockSetImmovable(x, y, z, true));
}

