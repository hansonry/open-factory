#include <stdint.h>
#include <ryanmock.h>
#include <GameEngine.h>
#include <GameData.h>
#include "CommonUtils.h"

void test_StrangePlatformFallingBug()
{
   struct commonTestBlockTypes types;
   util_fill_test_blocks(&types);

   util_ignore_ui_creation();
   util_ignore_ui_move();   

   GameEngine_Setup();

   GameEngine_BlockAdd(types.stickyImmovable, 1, 0, 1, eBF_North);
   GameEngine_BlockAdd(types.conveyor,        1, 1, 1, eBF_South);
   GameEngine_BlockAdd(types.conveyor,        1, 2, 1, eBF_South);
   GameEngine_BlockAdd(types.conveyor,        1, 3, 1, eBF_South);

  
   GameEngine_BlockAdd(types.sticky, 0, 1, 2, eBF_North);
   GameEngine_BlockAdd(types.sticky, 0, 2, 2, eBF_North);
   GameEngine_BlockAdd(types.sticky, 0, 3, 2, eBF_North);
   GameEngine_BlockAdd(types.sticky, 1, 1, 2, eBF_North);
   GameEngine_BlockAdd(types.sticky, 1, 2, 2, eBF_North);
   GameEngine_BlockAdd(types.sticky, 1, 3, 2, eBF_North);

   GameEngine_SetMode(eM_Playing);
   GameEngine_Step();
   GameEngine_SetMode(eM_Edit);

   GameEngine_BlockRemove(1, 3, 1);
   
   GameEngine_BlockAdd(types.conveyor, 1, 3, 1, eBF_South);
   
   GameEngine_SetMode(eM_Playing);
   GameEngine_SetMode(eM_Edit);
   GameEngine_SetMode(eM_Playing);
   GameEngine_Step();
   
   util_assert_block(types.conveyor, eBF_South, 1, 3, 1);

   GameEngine_Teardown();
}

int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest(test_StrangePlatformFallingBug),
   };
   return rmmRunTestsCmdLine(tests, "GameEngine", argc, args);
}


