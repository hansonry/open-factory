#include <stdint.h>
#include <stdlib.h>
#include <ryanmock.h>
#include <BlockEngine.h>
#include <GameData.h>
#include "CommonUtils.h"

static struct serializedBlock * StateData  = NULL;
static size_t                   StateCount = 0;

static inline
void util_state_save(void)
{
   StateCount = BlockEngine_GetBlockCount();
   StateData = calloc(StateCount, sizeof(struct serializedBlock));
   BlockEngine_WorldSerialize(StateData, StateCount);
}

static inline
void util_state_restore(void)
{
   BlockEngine_Clear();
   BlockEngine_WorldDeserialize(StateData, StateCount);
   free(StateData);
   StateData = NULL;
   StateCount = 0;
}


void test_Gravity(void)
{
   struct commonTestBlockTypes types;
   util_fill_test_blocks(&types);

   util_ignore_ui_creation();
   util_ignore_ui_move();   


   BlockEngine_Setup();

   BlockEngine_BlockAdd(types.notSticky, 0, 0, 1, eBF_North);
   BlockEngine_Step();
   util_assert_block(types.notSticky, eBF_North, 0, 0, 0);

   BlockEngine_Teardown();

}

void test_DontFallThoughWorld(void)
{
   struct commonTestBlockTypes types;
   util_fill_test_blocks(&types);

   util_ignore_ui_creation();

   BlockEngine_Setup();

   BlockEngine_BlockAdd(types.notSticky, 0, 0, 0, eBF_North);
   BlockEngine_Step();
   util_assert_block(types.notSticky, eBF_North, 0, 0, 0);

   BlockEngine_Teardown();

}

void test_StackFall(void)
{
   struct commonTestBlockTypes types;
   util_fill_test_blocks(&types);

   util_ignore_ui_creation();
   util_ignore_ui_move();   

   BlockEngine_Setup();

   BlockEngine_BlockAdd(types.notSticky, 0, 0, 2, eBF_North);
   BlockEngine_BlockAdd(types.notSticky, 0, 0, 3, eBF_North);
   BlockEngine_BlockAdd(types.notSticky, 0, 0, 4, eBF_North);
   BlockEngine_BlockAdd(types.notSticky, 0, 0, 5, eBF_North);
  
   BlockEngine_Step();
   BlockEngine_Step();
   
   util_assert_block(types.notSticky, eBF_North, 0, 0, 0);
   util_assert_block(types.notSticky, eBF_North, 0, 0, 1);
   util_assert_block(types.notSticky, eBF_North, 0, 0, 2);
   util_assert_block(types.notSticky, eBF_North, 0, 0, 3);

   BlockEngine_Teardown();

}

void test_ConveyorNorth(void)
{
   struct commonTestBlockTypes types;
   util_fill_test_blocks(&types);

   util_ignore_ui_creation();
   util_ignore_ui_move();   

   BlockEngine_Setup();

   BlockEngine_BlockAdd(types.conveyor, 0, 0, 0, eBF_North);
   BlockEngine_BlockAdd(types.grass,    0, 0, 1, eBF_North);
  
   BlockEngine_Step();
   
   util_assert_block(types.grass,    eBF_North, 0, -1, 1);
   util_assert_block(types.conveyor, eBF_North, 0,  0, 0);

   BlockEngine_Teardown();
}

void test_ConveyorEast(void)
{
   struct commonTestBlockTypes types;
   util_fill_test_blocks(&types);

   util_ignore_ui_creation();
   util_ignore_ui_move();   


   BlockEngine_Setup();

   BlockEngine_BlockAdd(types.conveyor, 0, 0, 0, eBF_East);
   BlockEngine_BlockAdd(types.grass,    0, 0, 1, eBF_North);
  
   BlockEngine_Step();
   
   util_assert_block(types.grass,    eBF_North, 1, 0, 1);
   util_assert_block(types.conveyor, eBF_East,  0, 0, 0);

   BlockEngine_Teardown();
}

void test_ConveyorSouth(void)
{
   struct commonTestBlockTypes types;
   util_fill_test_blocks(&types);

   util_ignore_ui_creation();
   util_ignore_ui_move();   

   BlockEngine_Setup();

   BlockEngine_BlockAdd(types.conveyor, 0, 0, 0, eBF_South);
   BlockEngine_BlockAdd(types.grass,    0, 0, 1, eBF_North);
  
   BlockEngine_Step();
   
   util_assert_block(types.grass,    eBF_North, 0, 1, 1);
   util_assert_block(types.conveyor, eBF_South, 0, 0, 0);

   BlockEngine_Teardown();
}

void test_ConveyorWest(void)
{
   struct commonTestBlockTypes types;
   util_fill_test_blocks(&types);

   util_ignore_ui_creation();
   util_ignore_ui_move();   

   BlockEngine_Setup();

   BlockEngine_BlockAdd(types.conveyor, 0, 0, 0, eBF_West);
   BlockEngine_BlockAdd(types.grass,    0, 0, 1, eBF_North);
  
   BlockEngine_Step();
   
   util_assert_block(types.grass,    eBF_North, -1, 0, 1);
   util_assert_block(types.conveyor, eBF_West,   0, 0, 0);

   BlockEngine_Teardown();
}

void test_ConveyorLoop(void)
{
   struct commonTestBlockTypes types;
   util_fill_test_blocks(&types);

   util_ignore_ui_creation();
   util_ignore_ui_move();   


   BlockEngine_Setup();

   // Conveyor Loop
   BlockEngine_BlockAdd(types.conveyor, 0,  0, 0, eBF_North);
   BlockEngine_BlockAdd(types.conveyor, 0, -1, 0, eBF_North);
   BlockEngine_BlockAdd(types.conveyor, 0, -2, 0, eBF_East);
   BlockEngine_BlockAdd(types.conveyor, 1, -2, 0, eBF_East);
   BlockEngine_BlockAdd(types.conveyor, 2, -2, 0, eBF_South);
   BlockEngine_BlockAdd(types.conveyor, 2, -1, 0, eBF_South);
   BlockEngine_BlockAdd(types.conveyor, 2,  0, 0, eBF_West);
   BlockEngine_BlockAdd(types.conveyor, 1,  0, 0, eBF_West);

   // Stuff on Top
   BlockEngine_BlockAdd(types.grass,     0,  0, 1, eBF_North);
   BlockEngine_BlockAdd(types.notSticky, 0, -1, 1, eBF_North);
   BlockEngine_BlockAdd(types.notSticky, 0, -2, 1, eBF_North);
   BlockEngine_BlockAdd(types.notSticky, 1, -2, 1, eBF_North);
   BlockEngine_BlockAdd(types.notSticky, 2, -2, 1, eBF_North);
   BlockEngine_BlockAdd(types.notSticky, 2, -1, 1, eBF_North);
   BlockEngine_BlockAdd(types.notSticky, 2,  0, 1, eBF_North);
   BlockEngine_BlockAdd(types.notSticky, 1,  0, 1, eBF_North);
   

   BlockEngine_Step();   
   util_assert_block(types.notSticky, eBF_North, 0,  0, 1);
   util_assert_block(types.grass,     eBF_North, 0, -1, 1);
   util_assert_block(types.notSticky, eBF_North, 0, -2, 1);
   util_assert_block(types.notSticky, eBF_North, 1, -2, 1);
   util_assert_block(types.notSticky, eBF_North, 2, -2, 1);
   util_assert_block(types.notSticky, eBF_North, 2, -1, 1);
   util_assert_block(types.notSticky, eBF_North, 2,  0, 1);
   util_assert_block(types.notSticky, eBF_North, 1,  0, 1);
   
   BlockEngine_Step();   
   util_assert_block(types.notSticky, eBF_North, 0,  0, 1);
   util_assert_block(types.notSticky, eBF_North, 0, -1, 1);
   util_assert_block(types.grass,     eBF_North, 0, -2, 1);
   util_assert_block(types.notSticky, eBF_North, 1, -2, 1);
   util_assert_block(types.notSticky, eBF_North, 2, -2, 1);
   util_assert_block(types.notSticky, eBF_North, 2, -1, 1);
   util_assert_block(types.notSticky, eBF_North, 2,  0, 1);
   util_assert_block(types.notSticky, eBF_North, 1,  0, 1);
   
   BlockEngine_Step();   
   util_assert_block(types.notSticky, eBF_North, 0,  0, 1);
   util_assert_block(types.notSticky, eBF_North, 0, -1, 1);
   util_assert_block(types.notSticky, eBF_North, 0, -2, 1);
   util_assert_block(types.grass,     eBF_North, 1, -2, 1);
   util_assert_block(types.notSticky, eBF_North, 2, -2, 1);
   util_assert_block(types.notSticky, eBF_North, 2, -1, 1);
   util_assert_block(types.notSticky, eBF_North, 2,  0, 1);
   util_assert_block(types.notSticky, eBF_North, 1,  0, 1);
   
   BlockEngine_Teardown();
}

void test_JoinTop(void)
{
   struct commonTestBlockTypes types;
   util_fill_test_blocks(&types);

   util_ignore_ui_creation();
   util_ignore_ui_move();   


   BlockEngine_Setup();

   BlockEngine_BlockAdd(types.conveyor, 0, 0, 0, eBF_North);
   BlockEngine_BlockAdd(types.grass,    0, 0, 1, eBF_North);
   BlockEngine_BlockAdd(types.grass,    0, 0, 2, eBF_North);
   BlockEngine_BlockAdd(types.grass,    0, 0, 3, eBF_North);
   BlockEngine_BlockAdd(types.grass,    0, 0, 4, eBF_North);
 
   rmmAssertTrue(BlockEngine_BlockJoin(0, 0, 1, 0, 0, 2));
   rmmAssertTrue(BlockEngine_BlockJoin(0, 0, 3, 0, 0, 4));
   rmmAssertTrue(BlockEngine_BlockJoin(0, 0, 3, 0, 0, 2));
 
   BlockEngine_Step();
   
   util_assert_block(types.conveyor, eBF_North, 0,  0, 0);
   util_assert_block(types.grass,    eBF_North, 0, -1, 1);
   util_assert_block(types.grass,    eBF_North, 0, -1, 2);
   util_assert_block(types.grass,    eBF_North, 0, -1, 3);
   util_assert_block(types.grass,    eBF_North, 0, -1, 4);

   BlockEngine_Teardown();
}

void test_JoinRight(void)
{
   struct commonTestBlockTypes types;
   util_fill_test_blocks(&types);

   util_ignore_ui_creation();
   util_ignore_ui_move();   


   BlockEngine_Setup();

   BlockEngine_BlockAdd(types.conveyor, 0, 0, 0, eBF_North);
   BlockEngine_BlockAdd(types.grass,    0, 0, 1, eBF_North);
   BlockEngine_BlockAdd(types.grass,    1, 0, 1, eBF_North);
   BlockEngine_BlockAdd(types.grass,    2, 0, 1, eBF_North);
   BlockEngine_BlockAdd(types.grass,    3, 0, 1, eBF_North);
 
   rmmAssertTrue(BlockEngine_BlockJoin(0, 0, 1, 1, 0, 1));
   rmmAssertTrue(BlockEngine_BlockJoin(2, 0, 1, 3, 0, 1));
   rmmAssertTrue(BlockEngine_BlockJoin(3, 0, 1, 0, 0, 1));
 
   BlockEngine_Step();
   
   util_assert_block(types.conveyor, eBF_North, 0,  0, 0);
   util_assert_block(types.grass,    eBF_North, 0, -1, 1);
   util_assert_block(types.grass,    eBF_North, 1, -1, 1);
   util_assert_block(types.grass,    eBF_North, 2, -1, 1);
   util_assert_block(types.grass,    eBF_North, 3, -1, 1);

   BlockEngine_Teardown();
}

void test_Remove(void)
{
   struct commonTestBlockTypes types;
   util_fill_test_blocks(&types);

   util_ignore_ui_creation();
   util_ignore_ui_move();   

   BlockEngine_Setup();

   BlockEngine_BlockAdd(types.notSticky, 0, 0, 0, eBF_North);
   rmmAssertTrue(BlockEngine_BlockRemove(0, 0, 0));
   util_assert_block(NULL, eBF_North, 0, 0, 0);

   BlockEngine_BlockAdd(types.notSticky, 0, 0, 0, eBF_North);
   BlockEngine_BlockAdd(types.notSticky, 1, 0, 0, eBF_North);
   rmmAssertTrue(BlockEngine_BlockRemove(1, 0, 0));
   util_assert_block(types.notSticky, eBF_North, 0, 0, 0);
   util_assert_block(NULL,  eBF_North, 1, 0, 0);
  
   BlockEngine_Teardown();
}

void test_SplitGroupIn2(void)
{
   struct commonTestBlockTypes types;
   util_fill_test_blocks(&types);

   util_ignore_ui_creation();
   util_ignore_ui_move();   


   BlockEngine_Setup();

   BlockEngine_BlockAdd(types.notSticky, 0, 0, 0, eBF_North);
   BlockEngine_BlockAdd(types.notSticky, 0, 0, 1, eBF_North);
   BlockEngine_BlockAdd(types.notSticky, 1, 0, 1, eBF_North);
   BlockEngine_BlockAdd(types.notSticky, 2, 0, 1, eBF_North);
   BlockEngine_BlockAdd(types.notSticky, 3, 0, 1, eBF_North);
 
   rmmAssertTrue(BlockEngine_BlockJoin(0, 0, 1, 1, 0, 1));
   rmmAssertTrue(BlockEngine_BlockJoin(2, 0, 1, 3, 0, 1));
   rmmAssertTrue(BlockEngine_BlockJoin(3, 0, 1, 0, 0, 1));

   rmmAssertTrue(BlockEngine_BlockRemove(2, 0, 1));

   
 
   BlockEngine_Step();
   
   util_assert_block(types.notSticky, eBF_North, 0, 0, 1);
   util_assert_block(types.notSticky, eBF_North, 1, 0, 1);
   util_assert_block(NULL,            eBF_North, 2, 0, 1);
   util_assert_block(NULL,            eBF_North, 2, 0, 0);
   util_assert_block(types.notSticky, eBF_North, 3, 0, 0);

   BlockEngine_Teardown();
}

void test_SplitGroupIn6(void)
{
   const int height = 10;
   struct commonTestBlockTypes types;
   util_fill_test_blocks(&types);

   util_ignore_ui_creation();
   util_ignore_ui_move();   

   BlockEngine_Setup();

   // Create 3D cross

   // Middle
   BlockEngine_BlockAdd(types.notSticky,  0,  0, height,     eBF_North);
   // Top and Bottom
   BlockEngine_BlockAdd(types.notSticky,  0,  0, height + 1, eBF_North);
   BlockEngine_BlockAdd(types.notSticky,  0,  0, height - 1, eBF_North);
   // Left Right
   BlockEngine_BlockAdd(types.notSticky,  1,  0, height,     eBF_North);
   BlockEngine_BlockAdd(types.notSticky, -1,  0, height,     eBF_North);
   // Front Back
   BlockEngine_BlockAdd(types.notSticky,  0,  1, height,     eBF_North);
   BlockEngine_BlockAdd(types.notSticky,  0, -1, height,     eBF_North);
 
   // Join Top and Bottom
   rmmAssertTrue(BlockEngine_BlockJoin(0, 0, height,  0,  0, height + 1));
   rmmAssertTrue(BlockEngine_BlockJoin(0, 0, height,  0,  0, height - 1));
   // Join Left and Right
   rmmAssertTrue(BlockEngine_BlockJoin(0, 0, height,  1,  0, height));
   rmmAssertTrue(BlockEngine_BlockJoin(0, 0, height, -1,  0, height));
   // Join Front and Back
   rmmAssertTrue(BlockEngine_BlockJoin(0, 0, height,  0,  1, height));
   rmmAssertTrue(BlockEngine_BlockJoin(0, 0, height,  0, -1, height));

   // Remove the center
   rmmAssertTrue(BlockEngine_BlockRemove(0, 0, height));


   // Add blocks to move stuff around to show that the 3D cross parts are
   // split.

   // Immovable block to prevent top from falling
   BlockEngine_BlockAdd(types.immovable, 0, 0, height, eBF_North);
   
   // Add a conveyor under Front, Back, Left, Right to verify that they move. 
   util_add_immovable_block(types.conveyor,  0, -1, height - 1, eBF_North);
   util_add_immovable_block(types.conveyor,  0,  1, height - 1, eBF_South);
   util_add_immovable_block(types.conveyor,  1,  0, height - 1, eBF_East);
   util_add_immovable_block(types.conveyor, -1,  0, height - 1, eBF_West);
   
   BlockEngine_Step();
   
   util_assert_block(types.notSticky, eBF_North,  0,  0, height + 1);
   util_assert_block(types.notSticky, eBF_North,  0,  2, height);
   util_assert_block(types.notSticky, eBF_North,  0, -2, height);
   util_assert_block(types.notSticky, eBF_North,  2,  0, height);
   util_assert_block(types.notSticky, eBF_North, -2,  0, height);
   util_assert_block(types.notSticky, eBF_North,  0,  0, height - 2);

   BlockEngine_Teardown();
}

void test_StickTop(void)
{
   struct commonTestBlockTypes types;
   util_fill_test_blocks(&types);

   util_ignore_ui_creation();
   util_ignore_ui_move();   

   BlockEngine_Setup();

   BlockEngine_BlockAdd(types.conveyor, 0, 0, 0, eBF_North);
   BlockEngine_BlockAdd(types.sticky,   0, 0, 1, eBF_North);
   BlockEngine_BlockAdd(types.sticky,   0, 0, 2, eBF_North);
  
   BlockEngine_Step();
   
   util_assert_block(types.conveyor, eBF_North, 0,  0, 0);
   util_assert_block(types.sticky,   eBF_North, 0, -1, 1);
   util_assert_block(types.sticky,   eBF_North, 0, -1, 2);

   BlockEngine_Teardown();
}

void test_LinearPush(void)
{
   struct commonTestBlockTypes types;
   util_fill_test_blocks(&types);

   util_ignore_ui_creation();
   util_ignore_ui_move();   

   BlockEngine_Setup();

   BlockEngine_BlockAdd(types.immovable, 0, 0, 0, eBF_North);
   BlockEngine_BlockAdd(types.conveyor,  0, 1, 0, eBF_North);
   BlockEngine_BlockAdd(types.notSticky, 0, 0, 1, eBF_North);
   BlockEngine_BlockAdd(types.notSticky, 0, 1, 1, eBF_North);
  
   BlockEngine_Step();
   
   util_assert_block(types.notSticky, eBF_North, 0, -1, 1);
   util_assert_block(types.notSticky, eBF_North, 0,  0, 1);

   BlockEngine_Teardown();
}

void test_Duplicator(void)
{
   struct commonTestBlockTypes types;
   util_fill_test_blocks(&types);

   util_ignore_ui_creation();
   util_ignore_ui_move();   

   BlockEngine_Setup();

   BlockEngine_BlockAdd(types.duplicator, 0, 0, 1, eBF_North);
   BlockEngine_BlockAdd(types.conveyor,   0, 0, 2, eBF_South);
  
   BlockEngine_Step();
   
   util_assert_block(types.conveyor, eBF_South, 0, 0, 0);

   BlockEngine_Teardown();
}

void test_GrinderNorth(void)
{
   struct commonTestBlockTypes types;
   util_fill_test_blocks(&types);

   util_ignore_ui_creation();
   util_ignore_ui_move();   

   BlockEngine_Setup();

   BlockEngine_BlockAdd(types.grinder, 0,  0, 0, eBF_North);
   BlockEngine_BlockAdd(types.grass,   0, -1, 1, eBF_North);
   
  
   BlockEngine_Step();
   
   util_assert_block(NULL, eBF_North, 0, -1, 0);
   util_assert_block(NULL, eBF_North, 0, -1, 1);
   
   BlockEngine_Teardown();
}

void test_GrinderEast(void)
{
   struct commonTestBlockTypes types;
   util_fill_test_blocks(&types);

   util_ignore_ui_creation();
   util_ignore_ui_move();   

   BlockEngine_Setup();

   BlockEngine_BlockAdd(types.grinder, 0, 0, 0, eBF_East);
   BlockEngine_BlockAdd(types.grass,   1, 0, 1, eBF_North);
   
  
   BlockEngine_Step();
   
   util_assert_block(NULL, eBF_North, 1, 0, 0);
   util_assert_block(NULL, eBF_North, 1, 0, 1);
   
   BlockEngine_Teardown();
}

void test_GrinderSouth(void)
{
   struct commonTestBlockTypes types;
   util_fill_test_blocks(&types);

   util_ignore_ui_creation();
   util_ignore_ui_move();   

   BlockEngine_Setup();

   BlockEngine_BlockAdd(types.grinder, 0, 0, 0, eBF_South);
   BlockEngine_BlockAdd(types.grass,   0, 1, 1, eBF_North);
   
  
   BlockEngine_Step();
   
   util_assert_block(NULL, eBF_North, 0, 1, 0);
   util_assert_block(NULL, eBF_North, 0, 1, 1);
   
   BlockEngine_Teardown();
}

void test_GrinderWest(void)
{
   struct commonTestBlockTypes types;
   util_fill_test_blocks(&types);

   util_ignore_ui_creation();
   util_ignore_ui_move();   

   BlockEngine_Setup();

   BlockEngine_BlockAdd(types.grinder,  0, 0, 0, eBF_West);
   BlockEngine_BlockAdd(types.grass,   -1, 0, 1, eBF_North);
   
  
   BlockEngine_Step();
   
   util_assert_block(NULL, eBF_North, -1, 0, 0);
   util_assert_block(NULL, eBF_North, -1, 0, 1);
   
   BlockEngine_Teardown();
}

void test_WelderNorth(void)
{
   struct commonTestBlockTypes types;
   util_fill_test_blocks(&types);

   util_ignore_ui_creation();
   util_ignore_ui_move();   

   BlockEngine_Setup();

   BlockEngine_BlockAdd(types.notSticky, 0, 0, 0, eBF_North);
   BlockEngine_BlockAdd(types.notSticky, 0, 0, 1, eBF_North);
   BlockEngine_BlockAdd(types.notSticky, 0, 0, 2, eBF_North);
   
   BlockEngine_BlockAdd(types.notSticky, 0, 1, 0, eBF_North);  
   BlockEngine_BlockAdd(types.welder,    0, 1, 1, eBF_North);  
   BlockEngine_BlockAdd(types.welder,    0, 1, 2, eBF_North);  

   BlockEngine_BlockAdd(types.conveyor,  1, 0, 0, eBF_West);
   BlockEngine_BlockAdd(types.conveyor,  2, 0, 0, eBF_West);
   BlockEngine_BlockAdd(types.notSticky, 2, 0, 1, eBF_North);  

   BlockEngine_Step();
   BlockEngine_Step();
   
   util_assert_block(types.notSticky, eBF_North, -1, 0, 1);
   util_assert_block(types.notSticky, eBF_North, -1, 0, 2);
   
   BlockEngine_Teardown();
}

void test_WelderEast(void)
{
   struct commonTestBlockTypes types;
   util_fill_test_blocks(&types);

   util_ignore_ui_creation();
   util_ignore_ui_move();   

   BlockEngine_Setup();

   BlockEngine_BlockAdd(types.notSticky,  0, 0, 0, eBF_North);
   BlockEngine_BlockAdd(types.notSticky,  0, 0, 1, eBF_North);
   BlockEngine_BlockAdd(types.notSticky,  0, 0, 2, eBF_North);
   
   BlockEngine_BlockAdd(types.notSticky, -1, 0, 0, eBF_North);  
   BlockEngine_BlockAdd(types.welder,    -1, 0, 1, eBF_East);  
   BlockEngine_BlockAdd(types.welder,    -1, 0, 2, eBF_East);  

   BlockEngine_BlockAdd(types.conveyor,   0, 1, 0, eBF_North);
   BlockEngine_BlockAdd(types.conveyor,   0, 2, 0, eBF_North);
   BlockEngine_BlockAdd(types.notSticky,  0, 2, 1, eBF_North);  

   BlockEngine_Step();
   BlockEngine_Step();
   
   util_assert_block(types.notSticky, eBF_North, 0, -1, 1);
   util_assert_block(types.notSticky, eBF_North, 0, -1, 2);
   
   BlockEngine_Teardown();
}

void test_WelderSouth(void)
{
   struct commonTestBlockTypes types;
   util_fill_test_blocks(&types);

   util_ignore_ui_creation();
   util_ignore_ui_move();   

   BlockEngine_Setup();

   BlockEngine_BlockAdd(types.notSticky, 0, 0, 0, eBF_North);
   BlockEngine_BlockAdd(types.notSticky, 0, 0, 1, eBF_North);
   BlockEngine_BlockAdd(types.notSticky, 0, 0, 2, eBF_North);
   
   BlockEngine_BlockAdd(types.notSticky, 0, -1, 0, eBF_North);  
   BlockEngine_BlockAdd(types.welder,    0, -1, 1, eBF_South);  
   BlockEngine_BlockAdd(types.welder,    0, -1, 2, eBF_South);  

   BlockEngine_BlockAdd(types.conveyor,  1, 0, 0, eBF_West);
   BlockEngine_BlockAdd(types.conveyor,  2, 0, 0, eBF_West);
   BlockEngine_BlockAdd(types.notSticky, 2, 0, 1, eBF_North);  

   BlockEngine_Step();
   BlockEngine_Step();
   
   util_assert_block(types.notSticky, eBF_North, -1, 0, 1);
   util_assert_block(types.notSticky, eBF_North, -1, 0, 2);
   
   BlockEngine_Teardown();
}

void test_WelderWest(void)
{
   struct commonTestBlockTypes types;
   util_fill_test_blocks(&types);

   util_ignore_ui_creation();
   util_ignore_ui_move();   

   BlockEngine_Setup();

   BlockEngine_BlockAdd(types.notSticky, 0, 0, 0, eBF_North);
   BlockEngine_BlockAdd(types.notSticky, 0, 0, 1, eBF_North);
   BlockEngine_BlockAdd(types.notSticky, 0, 0, 2, eBF_North);
   
   BlockEngine_BlockAdd(types.notSticky, 1, 0, 0, eBF_North);  
   BlockEngine_BlockAdd(types.welder,    1, 0, 1, eBF_West);  
   BlockEngine_BlockAdd(types.welder,    1, 0, 2, eBF_West);  

   BlockEngine_BlockAdd(types.conveyor,  0, 1, 0, eBF_North);
   BlockEngine_BlockAdd(types.conveyor,  0, 2, 0, eBF_North);
   BlockEngine_BlockAdd(types.notSticky, 0, 2, 1, eBF_North);  

   BlockEngine_Step();
   BlockEngine_Step();
   
   util_assert_block(types.notSticky, eBF_North, 0, -1, 1);
   util_assert_block(types.notSticky, eBF_North, 0, -1, 2);
   
   BlockEngine_Teardown();
}

void test_RotationAPISingleBlock(void)
{
   struct commonTestBlockTypes types;
   util_fill_test_blocks(&types);

   util_ignore_ui_creation();
   util_ignore_ui_move();   

   BlockEngine_Setup();

   BlockEngine_BlockAdd(types.notSticky, 0, 0, 0, eBF_North);

   BlockEngine_Rotate(0, 0, 0, eBR_Right);
   util_assert_block(types.notSticky, eBF_East, 0, 0, 0);
   
   BlockEngine_Rotate(0, 0, 0, eBR_Right);
   util_assert_block(types.notSticky, eBF_South, 0, 0, 0);
   
   BlockEngine_Rotate(0, 0, 0, eBR_Right);
   util_assert_block(types.notSticky, eBF_West, 0, 0, 0);
   
   BlockEngine_Rotate(0, 0, 0, eBR_Right);
   util_assert_block(types.notSticky, eBF_North, 0, 0, 0);
   
   BlockEngine_Rotate(0, 0, 0, eBR_Left);
   util_assert_block(types.notSticky, eBF_West, 0, 0, 0);
   
   BlockEngine_Rotate(0, 0, 0, eBR_Left);
   util_assert_block(types.notSticky, eBF_South, 0, 0, 0);
   
   BlockEngine_Rotate(0, 0, 0, eBR_Left);
   util_assert_block(types.notSticky, eBF_East, 0, 0, 0);
   
   BlockEngine_Rotate(0, 0, 0, eBR_Left);
   util_assert_block(types.notSticky, eBF_North, 0, 0, 0);
   
   BlockEngine_Teardown();
}

void test_RotationAPIThreeBlocksCenter(void)
{
   struct commonTestBlockTypes types;
   util_fill_test_blocks(&types);

   util_ignore_ui_creation();
   util_ignore_ui_move();   

   BlockEngine_Setup();

   BlockEngine_BlockAdd(types.notSticky, 0, 1, 0, eBF_North);
   BlockEngine_BlockAdd(types.notSticky, 0, 0, 0, eBF_North);
   BlockEngine_BlockAdd(types.notSticky, 1, 0, 0, eBF_North);

   BlockEngine_BlockJoin(0, 1, 0, 0, 0, 0);
   BlockEngine_BlockJoin(1, 0, 0, 0, 0, 0);


   BlockEngine_Rotate(0, 0, 0, eBR_Right);
   util_assert_block(types.notSticky, eBF_East,   0,  0, 0);
   util_assert_block(types.notSticky, eBF_East,  -1,  0, 0);
   util_assert_block(types.notSticky, eBF_East,   0,  1, 0);
   
   BlockEngine_Rotate(0, 0, 0, eBR_Right);
   util_assert_block(types.notSticky, eBF_South,  0,  0, 0);
   util_assert_block(types.notSticky, eBF_South, -1,  0, 0);
   util_assert_block(types.notSticky, eBF_South,  0, -1, 0);
   
   BlockEngine_Rotate(0, 0, 0, eBR_Right);
   util_assert_block(types.notSticky, eBF_West,   0,  0, 0);
   util_assert_block(types.notSticky, eBF_West,   1,  0, 0);
   util_assert_block(types.notSticky, eBF_West,   0, -1, 0);
   
   BlockEngine_Rotate(0, 0, 0, eBR_Right);
   util_assert_block(types.notSticky, eBF_North,  0,  0, 0);
   util_assert_block(types.notSticky, eBF_North,  1,  0, 0);
   util_assert_block(types.notSticky, eBF_North,  0,  1, 0);
   
   BlockEngine_Rotate(0, 0, 0, eBR_Left);
   util_assert_block(types.notSticky, eBF_West,   0,  0, 0);
   util_assert_block(types.notSticky, eBF_West,   1,  0, 0);
   util_assert_block(types.notSticky, eBF_West,   0, -1, 0);
   
   BlockEngine_Rotate(0, 0, 0, eBR_Left);
   util_assert_block(types.notSticky, eBF_South,  0,  0, 0);
   util_assert_block(types.notSticky, eBF_South, -1,  0, 0);
   util_assert_block(types.notSticky, eBF_South,  0, -1, 0);
   
   BlockEngine_Rotate(0, 0, 0, eBR_Left);
   util_assert_block(types.notSticky, eBF_East,   0,  0, 0);
   util_assert_block(types.notSticky, eBF_East,  -1,  0, 0);
   util_assert_block(types.notSticky, eBF_East,   0,  1, 0);
   
   BlockEngine_Rotate(0, 0, 0, eBR_Left);
   util_assert_block(types.notSticky, eBF_North,  0,  0, 0);
   util_assert_block(types.notSticky, eBF_North,  1,  0, 0);
   util_assert_block(types.notSticky, eBF_North,  0,  1, 0);
   
   BlockEngine_Teardown();
}

void test_RotationAPIThreeBlocksEdge(void)
{
   struct commonTestBlockTypes types;
   util_fill_test_blocks(&types);

   util_ignore_ui_creation();
   util_ignore_ui_move();   

   BlockEngine_Setup();

   BlockEngine_BlockAdd(types.notSticky, 0, 1, 0, eBF_North);
   BlockEngine_BlockAdd(types.notSticky, 0, 0, 0, eBF_North);
   BlockEngine_BlockAdd(types.notSticky, 1, 0, 0, eBF_North);

   BlockEngine_BlockJoin(0, 1, 0, 0, 0, 0);
   BlockEngine_BlockJoin(1, 0, 0, 0, 0, 0);


   BlockEngine_Rotate(1, 0, 0, eBR_Right);
   util_assert_block(types.notSticky, eBF_East,   1,  0, 0);
   util_assert_block(types.notSticky, eBF_East,   1, -1, 0);
   util_assert_block(types.notSticky, eBF_East,   0, -1, 0);
   
   BlockEngine_Rotate(1, 0, 0, eBR_Right);
   util_assert_block(types.notSticky, eBF_South,  1,  0, 0);
   util_assert_block(types.notSticky, eBF_South,  2,  0, 0);
   util_assert_block(types.notSticky, eBF_South,  2, -1, 0);
   
   BlockEngine_Rotate(1, 0, 0, eBR_Right);
   util_assert_block(types.notSticky, eBF_West,   1,  0, 0);
   util_assert_block(types.notSticky, eBF_West,   1,  1, 0);
   util_assert_block(types.notSticky, eBF_West,   2,  1, 0);
   
   BlockEngine_Rotate(1, 0, 0, eBR_Right);
   util_assert_block(types.notSticky, eBF_North,  1,  0, 0);
   util_assert_block(types.notSticky, eBF_North,  0,  0, 0);
   util_assert_block(types.notSticky, eBF_North,  0,  1, 0);
   
   BlockEngine_Rotate(1, 0, 0, eBR_Left);
   util_assert_block(types.notSticky, eBF_West,   1,  0, 0);
   util_assert_block(types.notSticky, eBF_West,   1,  1, 0);
   util_assert_block(types.notSticky, eBF_West,   2,  1, 0);
   
   BlockEngine_Rotate(1, 0, 0, eBR_Left);
   util_assert_block(types.notSticky, eBF_South,  1,  0, 0);
   util_assert_block(types.notSticky, eBF_South,  2,  0, 0);
   util_assert_block(types.notSticky, eBF_South,  2, -1, 0);
   
   BlockEngine_Rotate(1, 0, 0, eBR_Left);
   util_assert_block(types.notSticky, eBF_East,   1,  0, 0);
   util_assert_block(types.notSticky, eBF_East,   1, -1, 0);
   util_assert_block(types.notSticky, eBF_East,   0, -1, 0);
   
   BlockEngine_Rotate(1, 0, 0, eBR_Left);
   util_assert_block(types.notSticky, eBF_North,  1,  0, 0);
   util_assert_block(types.notSticky, eBF_North,  0,  0, 0);
   util_assert_block(types.notSticky, eBF_North,  0,  1, 0);
   
   BlockEngine_Teardown();
}

void test_RotatorCW(void)
{
   struct commonTestBlockTypes types;
   util_fill_test_blocks(&types);

   util_ignore_ui_creation();
   util_ignore_ui_move();   

   BlockEngine_Setup();

   BlockEngine_BlockAdd(types.rotatorCW, 0, 0, 0, eBF_North);
   BlockEngine_BlockAdd(types.notSticky, 0, 0, 1, eBF_North);
   BlockEngine_BlockAdd(types.notSticky, 1, 0, 1, eBF_North);
   
   // Conveyor belt to check for priority
   // It should be ignored
   BlockEngine_BlockAdd(types.conveyor, 1, 0, 0, eBF_East);

   BlockEngine_BlockJoin(0, 0, 1, 1, 0, 1);

   BlockEngine_Step();

   util_assert_block(types.rotatorCW, eBF_North,  0, 0, 0);
   util_assert_block(types.notSticky, eBF_East,   0, 0, 1);
   util_assert_block(types.notSticky, eBF_East,   0, 1, 1);
   
   BlockEngine_Step();

   util_assert_block(types.rotatorCW, eBF_North,  0, 0, 0);
   util_assert_block(types.notSticky, eBF_East,   0, 0, 1);
   util_assert_block(types.notSticky, eBF_East,   0, 1, 1);
   
   BlockEngine_Teardown();
}

void test_RotatorCWJoinIgnore(void)
{
   struct commonTestBlockTypes types;
   util_fill_test_blocks(&types);

   util_ignore_ui_creation();
   util_ignore_ui_move();   

   BlockEngine_Setup();

   BlockEngine_BlockAdd(types.rotatorCW, 0, 0, 0, eBF_North);
   BlockEngine_BlockAdd(types.notSticky, 0, 0, 1, eBF_North);
   BlockEngine_BlockAdd(types.notSticky, 1, 0, 1, eBF_North);
   
   BlockEngine_BlockJoin(0, 0, 1, 1, 0, 1);

   BlockEngine_Step();

   util_assert_block(types.rotatorCW, eBF_North,  0, 0, 0);
   util_assert_block(types.notSticky, eBF_East,   0, 0, 1);
   util_assert_block(types.notSticky, eBF_East,   0, 1, 1);

   // Attempt to join some blocks to trick the rotator to rotate again
   BlockEngine_BlockAdd(types.notSticky, 0, 0, 2, eBF_North);
   BlockEngine_BlockAdd(types.notSticky, 0, 1, 2, eBF_North);
   
   BlockEngine_BlockJoin(0, 0, 1, 0, 0, 2);
   BlockEngine_BlockJoin(0, 1, 2, 0, 0, 1);
   
   BlockEngine_Step();

   util_assert_block(types.rotatorCW, eBF_North,  0, 0, 0);
   util_assert_block(types.notSticky, eBF_East,   0, 0, 1);
   util_assert_block(types.notSticky, eBF_East,   0, 1, 1);
   util_assert_block(types.notSticky, eBF_North,  0, 0, 2);
   util_assert_block(types.notSticky, eBF_North,  0, 1, 2);
   
   BlockEngine_Teardown();
}

void test_RotatorCWStateRestore(void)
{
   struct commonTestBlockTypes types;
   util_fill_test_blocks(&types);

   util_ignore_ui_creation();
   util_ignore_ui_move();   

   BlockEngine_Setup();

   BlockEngine_BlockAdd(types.rotatorCW, 0, 0, 0, eBF_North);
   BlockEngine_BlockAdd(types.notSticky, 0, 0, 1, eBF_North);
   BlockEngine_BlockAdd(types.notSticky, 1, 0, 1, eBF_North);

   BlockEngine_BlockJoin(0, 0, 1, 1, 0, 1);

   util_state_save();
   util_state_restore();

   BlockEngine_Step();

   util_assert_block(types.rotatorCW, eBF_North,  0, 0, 0);
   util_assert_block(types.notSticky, eBF_East,   0, 0, 1);
   util_assert_block(types.notSticky, eBF_East,   0, 1, 1);
   
   util_state_save();
   util_state_restore();
   
   BlockEngine_Step();

   util_assert_block(types.rotatorCW, eBF_North,  0, 0, 0);
   util_assert_block(types.notSticky, eBF_East,   0, 0, 1);
   util_assert_block(types.notSticky, eBF_East,   0, 1, 1);
   
   BlockEngine_Teardown();
}

void test_RotatorCCW(void)
{
   struct commonTestBlockTypes types;
   util_fill_test_blocks(&types);

   util_ignore_ui_creation();
   util_ignore_ui_move();   

   BlockEngine_Setup();

   BlockEngine_BlockAdd(types.rotatorCCW, 0, 0, 0, eBF_North);
   BlockEngine_BlockAdd(types.notSticky,  0, 0, 1, eBF_North);
   BlockEngine_BlockAdd(types.notSticky,  1, 0, 1, eBF_North);
   
   // Conveyor belt to check for priority
   // It should be ignored
   BlockEngine_BlockAdd(types.conveyor, 1, 0, 0, eBF_East);

   BlockEngine_BlockJoin(0, 0, 1, 1, 0, 1);

   BlockEngine_Step();

   util_assert_block(types.rotatorCCW, eBF_North,  0, 0,  0);
   util_assert_block(types.notSticky,  eBF_West,   0, 0,  1);
   util_assert_block(types.notSticky,  eBF_West,   0, -1, 1);
   
   BlockEngine_Step();

   util_assert_block(types.rotatorCCW, eBF_North,  0, 0,  0);
   util_assert_block(types.notSticky,  eBF_West,   0, 0,  1);
   util_assert_block(types.notSticky,  eBF_West,   0, -1, 1);
   
   BlockEngine_Teardown();
}

void test_RotatorAfterRotator(void)
{
   struct commonTestBlockTypes types;
   util_fill_test_blocks(&types);

   util_ignore_ui_creation();
   util_ignore_ui_move();   

   BlockEngine_Setup();

   BlockEngine_BlockAdd(types.rotatorCW, 0, 0, 0, eBF_North);
   BlockEngine_BlockAdd(types.rotatorCW, 0, 1, 0, eBF_North);
   BlockEngine_BlockAdd(types.notSticky, 0, 0, 1, eBF_North);
   BlockEngine_BlockAdd(types.notSticky, 1, 0, 1, eBF_North);
   
   BlockEngine_BlockJoin(0, 0, 1, 1, 0, 1);

   BlockEngine_Step();

   util_assert_block(types.rotatorCW, eBF_North,  0, 0, 0);
   util_assert_block(types.notSticky, eBF_East,   0, 0, 1);
   util_assert_block(types.notSticky, eBF_East,   0, 1, 1);
   
   BlockEngine_Step();

   util_assert_block(types.rotatorCW, eBF_North,  0, 0, 0);
   util_assert_block(types.notSticky, eBF_South,  0, 1, 1);
   util_assert_block(types.notSticky, eBF_South,  1, 1, 1);
   
   BlockEngine_Step();

   util_assert_block(types.rotatorCW, eBF_North,  0, 0, 0);
   util_assert_block(types.notSticky, eBF_South,  0, 1, 1);
   util_assert_block(types.notSticky, eBF_South,  1, 1, 1);
   
   BlockEngine_Teardown();
}


void test_SaveAndLoad(void)
{
   struct commonTestBlockTypes types;
   util_fill_test_blocks(&types);

   util_ignore_ui_creation();
   util_ignore_ui_move();   

   BlockEngine_Setup();

   BlockEngine_BlockAdd(types.notSticky,  0, 0, 0, eBF_North);
   BlockEngine_BlockAdd(types.notSticky,  1, 0, 0, eBF_East);
   BlockEngine_BlockAdd(types.notSticky,  2, 0, 0, eBF_South);
   BlockEngine_BlockAdd(types.notSticky,  3, 0, 0, eBF_West);
   BlockEngine_BlockAdd(types.conveyor,   0, 1, 0, eBF_North);
   BlockEngine_BlockAdd(types.conveyor,   1, 1, 0, eBF_East);
   BlockEngine_BlockAdd(types.conveyor,   2, 1, 0, eBF_South);
   BlockEngine_BlockAdd(types.conveyor,   3, 1, 0, eBF_West);

   BlockEngine_BlockAdd(types.rotatorCW,  0, 2, 0, eBF_North);
   
   util_state_save();
   util_state_restore();

   util_assert_block(types.notSticky,  eBF_North,  0, 0, 0);
   util_assert_block(types.notSticky,  eBF_East,   1, 0, 0);
   util_assert_block(types.notSticky,  eBF_South,  2, 0, 0);
   util_assert_block(types.notSticky,  eBF_West,   3, 0, 0);
   
   util_assert_block(types.conveyor,  eBF_North,   0, 1, 0);
   util_assert_block(types.conveyor,  eBF_East,    1, 1, 0);
   util_assert_block(types.conveyor,  eBF_South,   2, 1, 0);
   util_assert_block(types.conveyor,  eBF_West,    3, 1, 0);
   
   util_assert_block(types.rotatorCW,  eBF_North,  0, 2, 0);
   
   BlockEngine_Teardown();
}

void test_SensorActivated(void)
{
   int sensorObj, dontCareObj;
   struct commonTestBlockTypes types;
   util_fill_test_blocks(&types);

   rmmMockBeginIgnoredAll(UIEngine_Entity_Move);
      rmmExpectAny(entity);
      rmmExpectAny(x);
      rmmExpectAny(y);
      rmmExpectAny(z);
   rmmMockEnd();
   rmmMockBeginIgnoredAll(UIEngine_Entity_Destroy);
      rmmExpectAny(entity);
   rmmMockEnd();
   
   rmmMockBeginIgnoredAll(UIEngine_Entity_Face);
      rmmExpectAny(entity);
      rmmExpectAny(facing);
   rmmMockEnd();

   rmmMockBeginOrdered(UIEngine_Entity_Create);
      rmmExpectPtrEqual(type, types.sensor);
   rmmMockEndReturnPtr(&sensorObj);

   rmmMockBeginOrdered(UIEngine_Entity_Create);
      rmmExpectPtrEqual(type, types.notSticky);
   rmmMockEndReturnPtr(&dontCareObj);


   BlockEngine_Setup();

   BlockEngine_BlockAdd(types.sensor,    0,  0, 0, eBF_North);
   BlockEngine_BlockAdd(types.notSticky, 0, -1, 0, eBF_North);
  
   rmmMockBeginOrdered(UIEngine_Entity_Activate);
      rmmExpectPtrEqual(entity, &sensorObj);
      rmmExpectTrue(isActive);
   rmmMockEnd();

   BlockEngine_Step();

   BlockEngine_BlockRemove(0, -1, 0);
   rmmMockBeginOrdered(UIEngine_Entity_Activate);
      rmmExpectPtrEqual(entity, &sensorObj);
      rmmExpectFalse(isActive);
   rmmMockEnd();
   
   BlockEngine_Step();
   
   BlockEngine_Teardown();
}

void test_SensorAndWireActivated(void)
{
   int dontCareObj;
   int sensorObj[2];
   int wireObj[4];
   struct commonTestBlockTypes types;
   util_fill_test_blocks(&types);

   rmmMockBeginIgnoredAll(UIEngine_Entity_Move);
      rmmExpectAny(entity);
      rmmExpectAny(x);
      rmmExpectAny(y);
      rmmExpectAny(z);
   rmmMockEnd();
   rmmMockBeginIgnoredAll(UIEngine_Entity_Destroy);
      rmmExpectAny(entity);
   rmmMockEnd();
   
   rmmMockBeginIgnoredAll(UIEngine_Entity_Face);
      rmmExpectAny(entity);
      rmmExpectAny(facing);
   rmmMockEnd();

   rmmMockBeginOrdered(UIEngine_Entity_Create);
      rmmExpectPtrEqual(type, types.sensor);
   rmmMockEndReturnPtr(&sensorObj[0]);
   rmmMockBeginOrdered(UIEngine_Entity_Create);
      rmmExpectPtrEqual(type, types.wire);
   rmmMockEndReturnPtr(&wireObj[0]);
   rmmMockBeginOrdered(UIEngine_Entity_Create);
      rmmExpectPtrEqual(type, types.wire);
   rmmMockEndReturnPtr(&wireObj[1]);
   
   rmmMockBeginOrdered(UIEngine_Entity_Create);
      rmmExpectPtrEqual(type, types.sensor);
   rmmMockEndReturnPtr(&sensorObj[1]);
   rmmMockBeginOrdered(UIEngine_Entity_Create);
      rmmExpectPtrEqual(type, types.wire);
   rmmMockEndReturnPtr(&wireObj[2]);
   rmmMockBeginOrdered(UIEngine_Entity_Create);
      rmmExpectPtrEqual(type, types.wire);
   rmmMockEndReturnPtr(&wireObj[3]);
   
   rmmMockBeginOrdered(UIEngine_Entity_Create);
      rmmExpectPtrEqual(type, types.notSticky);
   rmmMockEndReturnPtr(&dontCareObj);


   BlockEngine_Setup();

   BlockEngine_BlockAdd(types.sensor,    0,  0, 0, eBF_North);
   BlockEngine_BlockAdd(types.wire,      0,  1, 0, eBF_North);
   BlockEngine_BlockAdd(types.wire,      1,  1, 0, eBF_North);

   BlockEngine_BlockAdd(types.sensor,    3,  1, 0, eBF_North);
   BlockEngine_BlockAdd(types.wire,      3,  2, 0, eBF_North);
   BlockEngine_BlockAdd(types.wire,      2,  2, 0, eBF_North);
   
   BlockEngine_BlockAdd(types.notSticky, 0, -1, 0, eBF_North);
  
   rmmMockBeginOrdered(UIEngine_Entity_Activate);
      rmmExpectPtrEqual(entity, &wireObj[1]);
      rmmExpectTrue(isActive);
   rmmMockEnd();
   rmmMockBeginOrdered(UIEngine_Entity_Activate);
      rmmExpectPtrEqual(entity, &wireObj[0]);
      rmmExpectTrue(isActive);
   rmmMockEnd();
   rmmMockBeginOrdered(UIEngine_Entity_Activate);
      rmmExpectPtrEqual(entity, &sensorObj[0]);
      rmmExpectTrue(isActive);
   rmmMockEnd();

   BlockEngine_Step();

   rmmMockBeginOrdered(UIEngine_Entity_Create);
      rmmExpectPtrEqual(type, types.notSticky);
   rmmMockEndReturnPtr(&dontCareObj);

   BlockEngine_BlockRemove(0, -1, 0);
   BlockEngine_BlockAdd(types.notSticky, 3, 0, 0, eBF_North);
   
   rmmMockBeginOrdered(UIEngine_Entity_Activate);
      rmmExpectPtrEqual(entity, &wireObj[1]);
      rmmExpectFalse(isActive);
   rmmMockEnd();
   rmmMockBeginOrdered(UIEngine_Entity_Activate);
      rmmExpectPtrEqual(entity, &wireObj[0]);
      rmmExpectFalse(isActive);
   rmmMockEnd();
   rmmMockBeginOrdered(UIEngine_Entity_Activate);
      rmmExpectPtrEqual(entity, &sensorObj[0]);
      rmmExpectFalse(isActive);
   rmmMockEnd();
   
   rmmMockBeginOrdered(UIEngine_Entity_Activate);
      rmmExpectPtrEqual(entity, &wireObj[3]);
      rmmExpectTrue(isActive);
   rmmMockEnd();
   rmmMockBeginOrdered(UIEngine_Entity_Activate);
      rmmExpectPtrEqual(entity, &wireObj[2]);
      rmmExpectTrue(isActive);
   rmmMockEnd();
   rmmMockBeginOrdered(UIEngine_Entity_Activate);
      rmmExpectPtrEqual(entity, &sensorObj[1]);
      rmmExpectTrue(isActive);
   rmmMockEnd();
   
   BlockEngine_Step();
   
   BlockEngine_Teardown();
}




int main(int argc, char * args[])
{
   struct ryanmock_test tests[] = {
      rmmMakeTest(test_Gravity),
      rmmMakeTest(test_DontFallThoughWorld),
      rmmMakeTest(test_StackFall),
      rmmMakeTest(test_ConveyorNorth),
      rmmMakeTest(test_ConveyorEast),
      rmmMakeTest(test_ConveyorSouth),
      rmmMakeTest(test_ConveyorWest),
      rmmMakeTest(test_ConveyorLoop),
      rmmMakeTest(test_JoinTop),
      rmmMakeTest(test_JoinRight),
      rmmMakeTest(test_Remove),
      rmmMakeTest(test_SplitGroupIn2),
      rmmMakeTest(test_SplitGroupIn6),
      rmmMakeTest(test_StickTop),
      rmmMakeTest(test_LinearPush),
      rmmMakeTest(test_Duplicator),
      rmmMakeTest(test_GrinderNorth),
      rmmMakeTest(test_GrinderEast),
      rmmMakeTest(test_GrinderSouth),
      rmmMakeTest(test_GrinderWest),
      rmmMakeTest(test_WelderNorth),
      rmmMakeTest(test_WelderEast),
      rmmMakeTest(test_WelderSouth),
      rmmMakeTest(test_WelderWest),
      rmmMakeTest(test_RotationAPISingleBlock),
      rmmMakeTest(test_RotationAPIThreeBlocksCenter),
      rmmMakeTest(test_RotationAPIThreeBlocksEdge),
      rmmMakeTest(test_RotatorCW),
      rmmMakeTest(test_RotatorCWJoinIgnore),
      rmmMakeTest(test_RotatorCWStateRestore),
      rmmMakeTest(test_RotatorAfterRotator),
      rmmMakeTest(test_RotatorCCW),
      rmmMakeTest(test_SaveAndLoad),
      rmmMakeTest(test_SensorActivated),
      rmmMakeTest(test_SensorAndWireActivated),
   };
   return rmmRunTestsCmdLine(tests, "BlockEngine", argc, args);
}


