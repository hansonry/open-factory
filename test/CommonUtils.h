#ifndef __COMMONUTILS_H__
#define __COMMONUTILS_H__
#include <BlockEngine.h>

void util_ignore_ui_creation(void);

void util_ignore_ui_move(void);

struct commonTestBlockTypes
{
   const struct blockType * conveyor;
   const struct blockType * duplicator;
   const struct blockType * grinder;
   const struct blockType * welder;
   const struct blockType * immovable;
   const struct blockType * notSticky;
   const struct blockType * sticky;
   const struct blockType * stickyImmovable;
   const struct blockType * grass;
   const struct blockType * rotatorCW;
   const struct blockType * rotatorCCW;
   const struct blockType * wire;
   const struct blockType * sensor;
   const struct blockType * pusher;
};

#define util_assert_block(expectedType, expectedFacing, x, y, z) \
rmmEnterFunction(util_assert_block);                             \
_util_assert_block(expectedType, expectedFacing, x, y, z);       \
rmmExitFunction(util_assert_block)

void _util_assert_block(const struct blockType * expectedType, 
                        enum blockFacing expectedFacing,
                        int x, int y, int z);

void util_fill_test_blocks(struct commonTestBlockTypes * types);


void util_add_immovable_block(const struct blockType * type, 
                              int x, int y, int z, enum blockFacing facing);


#endif // __COMMONUTILS_H__

