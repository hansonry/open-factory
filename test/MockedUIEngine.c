#include <ryanmock.h>
#include <UIEngine.h>

bool UIEngine_Setup(void)
{
   rmmFunctionCalled();
   rmmMockReturn(bool);
}

void UIEngine_Teardown(void)
{
   rmmFunctionCalled();
}

bool UIEngine_IsRunning(void)
{
   rmmFunctionCalled();
   rmmMockReturn(bool);
}

void UIEngine_Step(void)
{
   rmmFunctionCalled();
}

const struct blockType * UIEngine_GetSelectedBlockType(void)
{
   rmmFunctionCalled();
   rmmMockReturn(const struct blockType *);
}

struct uiEngineEntity * UIEngine_Entity_Create(const struct blockType * type)
{
   rmmFunctionCalled();
   rmmParamCheck(type);
   rmmMockReturn(struct uiEngineEntity *);
}

void UIEngine_Entity_Destroy(struct uiEngineEntity * entity)
{
   rmmFunctionCalled();
   rmmParamCheck(entity);
}


void UIEngine_Entity_Move(struct uiEngineEntity * entity, int x, int y, int z)
{
   rmmFunctionCalled();
   rmmParamCheck(entity);
   rmmParamCheck(x);
   rmmParamCheck(y);
   rmmParamCheck(z);
}
void UIEngine_Entity_Face(struct uiEngineEntity * entity, 
                          enum blockFacing facing)
{
   rmmFunctionCalled();
   rmmParamCheck(entity);
   rmmParamCheck(facing);
}

void UIEngine_Entity_Activate(struct uiEngineEntity * entity, bool isActive)
{
   rmmFunctionCalled();
   rmmParamCheck(entity);
   rmmParamCheck(isActive);
}


const struct blockType * UIEngine_Entity_GetType(struct uiEngineEntity * entity)
{
   rmmFunctionCalled();
   rmmParamCheck(entity);
   rmmMockReturn(const struct blockType *);
}



