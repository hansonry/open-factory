# Open Factory

A console remake of Infinifactory using NCurses.

![Example](doc/assets/MakeAndDestroy.gif)
![WeldingExample](doc/assets/welder_example.gif)

## Controls

* `q`     - Quit
* `Up`    - Cursor Up
* `Down`  - Cursor Down
* `Left`  - Cursor Left
* `Right` - Cursor Down
* `<`     - Z Level Up
* `>`     - Z Level Down
* `a`     - Previous Selection
* `d`     - Next Selection
* `w`     - Place Selected Block
* `s`     - Remove Selected Block
* `e`     - Set Selected block to block under cursor
* `Space` - Play/Reset the simulation

Press any key to move the simulation forward.


## Building

You are going to need the folowing tools:

* Git
* CMake
* Ncurses Library

**Linux**

Here are the steps:

1. Check out the project.
2. Update the submodule.
3. Run `cmake .`
4. Run `make`.


**Windows**

There isn't a good way to do this yet. I am sure it is possible.



