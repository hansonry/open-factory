#ifndef __BLOCKENGINE_H__
#define __BLOCKENGINE_H__
#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>
#include "GameData.h"

enum blockFacing
{
   eBF_North,
   eBF_East,
   eBF_South,
   eBF_West
};

enum blockRotation
{
   eBR_Right,
   eBR_Left
};

void BlockEngine_Setup(void);
void BlockEngine_Teardown(void);

void BlockEngine_Step(void);

void BlockEngine_Clear(void);

const struct blockType * BlockEngine_GetBlockAt(int x, int y, int z, 
                                               enum blockFacing * facing);

void BlockEngine_BlockAdd(const struct blockType * type, int x, int y, int z, 
                         enum blockFacing facing);
bool BlockEngine_BlockRemove(int x, int y, int z);
bool BlockEngine_BlockJoin(int x1, int y1, int z1, int x2, int y2, int z2);
bool BlockEngine_BlockSetImmovable(int x, int y, int z, bool immovable);

bool BlockEngine_Rotate(int x, int y, int z, enum blockRotation rotation);

#define BLOCKENGINE_FLAG_NONE          0x00
#define BLOCKENGINE_FLAG_ROTATOR_DONE  0x01

struct serializedBlock
{
   int32_t x;
   int32_t y;
   int32_t z;
   const struct blockType * type;
   uint8_t immovable;
   uint8_t facing;
   uint32_t groupIndex;
   uint8_t flags;
};

size_t BlockEngine_GetBlockCount(void);

size_t BlockEngine_WorldSerialize(struct serializedBlock * blocks, size_t size);
void  BlockEngine_WorldDeserialize(const struct serializedBlock * blocks, size_t size);


#endif // __BLOCKENGINE_H__


