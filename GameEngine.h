#ifndef __GAMEENGINE_H__
#define __GAMEENGINE_H__
#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>
#include "GameData.h"
#include "BlockEngine.h"

enum mode
{
   eM_Edit,
   eM_Paused,
   eM_Playing,
};

void GameEngine_Setup(void);
void GameEngine_Teardown(void);

void GameEngine_SetMode(enum mode mode);
enum mode GameEngine_GetMode(void);

void GameEngine_Step(void);

void GameEngine_Clear(void);

const struct blockType * GameEngine_GetBlockAt(int x, int y, int z, 
                                               enum blockFacing * facing);

void GameEngine_BlockAdd(const struct blockType * type, int x, int y, int z, 
                         enum blockFacing facing);
bool GameEngine_BlockRemove(int x, int y, int z);
bool GameEngine_BlockJoin(int x1, int y1, int z1, int x2, int y2, int z2);
bool GameEngine_BlockSetImmovable(int x, int y, int z, bool immovable);

#endif // __GAMEENGINE_H__


