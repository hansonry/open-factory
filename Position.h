#ifndef __POSITION_H__
#define __POSITION_H__
#include "GameEngine.h"

struct position
{
   int x;
   int y;
   int z;
};

static inline
struct position * PositionSet(struct position * position, int x, int y, int z)
{
   position->x = x;
   position->y = y;
   position->z = z;
   return position;
}

static inline
struct position * PositionCopy(struct position * dest, const struct position * src)
{
   dest->x = src->x;
   dest->y = src->y;
   dest->z = src->z;
   return dest;
}


static inline
struct position * PositionAdd(struct position * position, const struct position * a)
{
   position->x += a->x;
   position->y += a->y;
   position->z += a->z;
   return position;
}

static inline
struct position * PositionAddInt(struct position * position, int x, int y, int z)
{
   position->x += x;
   position->y += y;
   position->z += z;
   return position;
}

static inline
struct position * PositionSubtract(struct position * position, const struct position * b)
{
   position->x -= b->x;
   position->y -= b->y;
   position->z -= b->z;
   return position;
}

static inline
bool PositionIsEqual(const struct position * p1, const struct position * p2)
{
   return p1->x == p2->x &&
          p1->y == p2->y &&
          p1->z == p2->z;
}

static inline
unsigned int IntABS(int value)
{
   if(value < 0)
   {
      return -value;
   }
   return value;
}

static inline
unsigned int PositionManhattanLength(const struct position * position)
{
   unsigned int x, y, z;
   x = IntABS(position->x); 
   y = IntABS(position->y); 
   z = IntABS(position->z); 
   return x + y + z;
}

static inline
struct position * PositionRotate(struct position * pos, enum blockFacing facing)
{
   int prevX = pos->x;
   int prevY = pos->y;
   switch(facing)
   {
   case eBF_North:
      // Do Nothing on purpose
      break;
   case eBF_East:
      pos->x = -prevY;
      pos->y =  prevX;
      break;
   case eBF_South:
      pos->x = -prevX;
      pos->y = -prevY;
      break;
   case eBF_West:
      pos->x =  prevY;
      pos->y = -prevX;
      break;
   }
   return pos;
}

static inline
struct position * PositionGetRotatedPoint(struct position * pos, 
                                          int x, int y, int z, 
                                          enum blockFacing facing)
{
   PositionSet(pos, x, y, z);
   PositionRotate(pos, facing);
   return pos;
}

#endif // __POSITION_H__

