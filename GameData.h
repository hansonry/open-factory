#ifndef __GAMEDATA_H__
#define __GAMEDATA_H__
#include <stdint.h>
#include <stdbool.h>


enum blockDirections
{
   eBD_XPos,
   eBD_XNeg,
   eBD_YPos,
   eBD_YNeg,
   eBD_ZPos,
   eBD_ZNeg,
   eBD_Last
};

#define BLOCKFLAGS_NONE   0
#define BLOCKFLAGS_STICKY 1

struct blockType
{
   const char * name;
   bool immovable;
   uint8_t blockFlags[eBD_Last];
};


static inline 
bool BlockFlag_IsSticky(uint8_t flag)
{
   return (flag & BLOCKFLAGS_STICKY) == BLOCKFLAGS_STICKY;
}


const struct blockType * GameData_GetAllTypes(unsigned int * count);
const struct blockType * GameData_GetTypeByName(const char * name);
unsigned int GameData_GetIndex(const struct blockType * type);


#endif // __GAMEDATA_H__


